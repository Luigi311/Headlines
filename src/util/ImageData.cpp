#include "ImageData.h"
#include "Helpers.h"

namespace util
{
    std::string ImageData::GetFSPath() const
    {
        if (m_Url.empty())
        {
            return "";
        }
        std::string imageFileName = util::Helpers::UnescapeHtml(m_Url);
        std::vector<std::string> imageUrlSplit;
        util::Helpers::SplitString(imageFileName, imageUrlSplit, '/');
        imageFileName = imageUrlSplit[imageUrlSplit.size() - 1];

        if (imageFileName.find('?') != std::string::npos)
        {
            imageFileName.resize(imageFileName.find('?'));
        }

        unsigned long fileExtensionIndex = -1;
        auto it = imageFileName.find('.');
        while (it != std::string::npos)
        {
            fileExtensionIndex = it;
            it = imageFileName.find('.', it + 1);
        }

        std::string name = imageFileName.substr(0, fileExtensionIndex);
        std::string extension = imageFileName.substr(fileExtensionIndex, imageFileName.size() - fileExtensionIndex);

#if !WIN32
        imageFileName = util::Helpers::FormatString("%s/headlines/%s%dx%d%s", Glib::get_user_cache_dir().c_str(), name.c_str(), m_Width, m_Height, extension.c_str());
#else
        imageFileName = util::Helpers::FormatString("%s%dx%d", name.c_str(), m_Width, m_Height, extension.c_str());
#endif

        return imageFileName;
    }

    ImageData::ImageData(const std::string& imageUrl, int width, int height)
            : m_Url(imageUrl)
              , m_Width(width)
              , m_Height(height)
    {
    }

    ImageData::ImageData()
    {
    }

    const ImageData& ImageCollection::GetImageData(ImageQuality quality) const
    {
        int index = 0;
        switch (quality)
        {
            case ImageQuality::Lowest:
                index = 0;
                break;
            case ImageQuality::Low:
                index = std::min(1, (int)m_Images.size() - 1);
                break;
            case ImageQuality::Medium:
                index = m_Images.size() > 1 ? m_Images.size() / 2 : 0;
                break;
            case ImageQuality::High:
                index = std::max(0, (int)m_Images.size() - 2);
                break;
            case ImageQuality::Highest:
                index = std::max(0, (int)m_Images.size() - 1);
                break;
        }
        return m_Images[index];
    }
}