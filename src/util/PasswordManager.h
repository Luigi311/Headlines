#pragma once

#include <libsecret/secret.h>

namespace util
{
    class PasswordManager
    {
    public:
        static void SaveToken(const std::string& token);
        static void SaveRefreshToken(const std::string& refreshToken);
        static void SaveTokenExpiry(uint64_t expiry);

        static std::string LoadToken();
        static std::string LoadRefreshToken();
        static uint64_t LoadTokenExpiry();
    };
}
