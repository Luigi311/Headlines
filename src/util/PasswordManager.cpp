#include "PasswordManager.h"

namespace util
{
    static const SecretSchema* GetSchema()
    {
        static const SecretSchema schema = {
                "caveman250.Headlines.Password", SECRET_SCHEMA_NONE,
                {
                        {  "type", SECRET_SCHEMA_ATTRIBUTE_STRING },
                        { "NULL", SECRET_SCHEMA_ATTRIBUTE_STRING },
                    },
                0, NULL, NULL, NULL, NULL, NULL, NULL, NULL
        };

        return &schema;
    }

#define CHECK_ERROR() \
if (!VERIFY(error == nullptr, "%s", error->message))\
{\
    g_error_free(error);\
    error = nullptr;\
}

    static SecretCollection* GetSecretCollection(SecretService* service, bool createCollectionIfMissing)
    {
        GError* error = nullptr;
        SecretCollection* collection = secret_collection_for_alias_sync(service, "default", SECRET_COLLECTION_LOAD_ITEMS, nullptr, &error);
        CHECK_ERROR()

        if (!collection && createCollectionIfMissing)
        {
            collection = secret_collection_create_sync(service, "Default Keyring", "default", SECRET_COLLECTION_CREATE_NONE, nullptr, &error);
            CHECK_ERROR()
        }

        return collection;
    }

    static SecretCollection* EnsureCollectionUnlocked(SecretService* service, SecretCollection* collection)
    {
        if (secret_collection_get_locked(collection))
        {
            GError* error = nullptr;
            GList* unlocked = nullptr;
            GList* list = g_list_alloc();
            list->data = collection;
            secret_service_unlock_sync(service, list, nullptr, &unlocked, &error);
            CHECK_ERROR()
            g_list_free(list);

            if (VERIFY(unlocked, "Failed to unlock default keyring"))
            {
                SecretCollection* unlockedCollection =  (SecretCollection*)unlocked->data;
                CHECK_ERROR()
                g_list_free(unlocked);
                return unlockedCollection;
            }

            return nullptr;
        }
        else
        {
            return collection;
        }
    }

    static SecretItem* EnsureItemUnlocked(SecretService* service, SecretItem* item)
    {
        if (secret_item_get_locked(item))
        {
            GError* error = nullptr;
            GList* unlocked = nullptr;
            GList* list = g_list_alloc();
            list->data = item;
            secret_service_unlock_sync(service, list, nullptr, &unlocked, &error);
            CHECK_ERROR()
            g_list_free(list);

            if (VERIFY(unlocked, "Failed to unlock secret item"))
            {
                SecretItem* unlockedItem = (SecretItem*)unlocked->data;
                CHECK_ERROR()
                g_list_free(unlocked);

                secret_item_load_secret_sync(unlockedItem, nullptr, &error);
                CHECK_ERROR()
                return unlockedItem;
            }

            return nullptr;
        }
        else
        {
            return item;
        }
    }

    static std::string GetSecret(const std::string& type)
    {
        GError* error = nullptr;
        SecretService* service = secret_service_get_sync(SECRET_SERVICE_LOAD_COLLECTIONS, nullptr, &error);
        CHECK_ERROR()

        SecretCollection* collection = GetSecretCollection(service, false);
        if (!collection)
        {
            return "";
        }

        GHashTable* attr = secret_attributes_build(GetSchema(), "type", type.c_str(), nullptr);
        GList* secrets = secret_collection_search_sync(collection, GetSchema(), attr, SECRET_SEARCH_LOAD_SECRETS, nullptr, &error);
        CHECK_ERROR()

        if (!secrets)
        {
            return "";
        }
        else
        {
            SecretItem* item = (SecretItem*)secrets->data;
            item = EnsureItemUnlocked(service, item);
            SecretValue* value = item ? secret_item_get_secret(item) : nullptr;
            return value ? secret_value_get_text(value) : "";
        }
    }

    static void SetSecret(const std::string& type, const std::string& value)
    {
        GError* error = nullptr;
        SecretService* service = secret_service_get_sync(SECRET_SERVICE_LOAD_COLLECTIONS, nullptr, &error);
        CHECK_ERROR()

        SecretCollection* collection = GetSecretCollection(service, true);
        collection = EnsureCollectionUnlocked(service, collection);
        if (!collection)
        {
            return;
        }
        GHashTable* attr = secret_attributes_build(GetSchema(), "type", type.c_str(), nullptr);
        GList* secrets = secret_collection_search_sync(collection, GetSchema(), attr, SECRET_SEARCH_LOAD_SECRETS, nullptr, &error);
        CHECK_ERROR()
        if (!secrets)
        {
            SecretValue* newValue = secret_value_new(value.c_str(), -1, "text/plain");
            std::stringstream ss;
            ss << "Headlines - " << type;
            secret_item_create_sync(collection, GetSchema(), attr, ss.str().c_str(), newValue, SECRET_ITEM_CREATE_NONE, nullptr, &error);
            CHECK_ERROR()
        }
        else
        {
            SecretItem* item = (SecretItem*)secrets->data;
            SecretValue* newValue = secret_value_new(value.c_str(), -1, "text/plain");
            secret_item_set_secret_sync(item, newValue, nullptr, &error);
            CHECK_ERROR()
        }
    }

    std::string PasswordManager::LoadToken()
    {
        return GetSecret("token");
    }

    std::string PasswordManager::LoadRefreshToken()
    {
        return GetSecret("refresh_token");
    }

    uint64_t PasswordManager::LoadTokenExpiry()
    {
        std::string secret = GetSecret("token_expiry");

        if (secret.empty())
        {
            return 0;
        }

        uint64_t ret;
        std::istringstream iss(secret);
        iss >> ret;

        return ret;
    }

    void PasswordManager::SaveToken(const std::string& token)
    {
        SetSecret("token", token);
    }

    void PasswordManager::SaveRefreshToken(const std::string& refreshToken)
    {
        SetSecret("refresh_token", refreshToken);
    }

    void PasswordManager::SaveTokenExpiry(uint64_t expiry)
    {
        std::ostringstream oss;
        oss << expiry;

        SetSecret("token_expiry", oss.str());
    }
}