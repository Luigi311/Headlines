#pragma once

namespace api
{
    class SubredditIconCache
    {
    public:
        static SubredditIconCache* Get();

        bool IsIconCached(const std::string& subredditName);
        std::string GetSubredditIcon(const std::string& subredditName);
        void SetSubredditIcon(const std::string& subredditName, const std::string& iconPath);
    private:
        void Load();
        void Save();
        std::string GetFilePath();

        static SubredditIconCache* s_Instance;

        std::mutex m_Mutex;
        std::unordered_map<std::string, std::string> m_SubredditIcons;
    };
}

