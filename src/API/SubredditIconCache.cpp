#include "SubredditIconCache.h"

namespace api
{
    SubredditIconCache* SubredditIconCache::s_Instance = nullptr;

    SubredditIconCache* SubredditIconCache::Get()
    {
        if (!s_Instance)
        {
            s_Instance = new SubredditIconCache();
            s_Instance->Load();
        }

        return s_Instance;
    }

    bool SubredditIconCache::IsIconCached(const std::string& subredditName)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        return m_SubredditIcons.count(subredditName) > 0;
    }

    std::string SubredditIconCache::GetSubredditIcon(const std::string& subredditName)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        if (m_SubredditIcons.count(subredditName) > 0)
        {
            return m_SubredditIcons[subredditName];
        }

        return "";
    }

    void SubredditIconCache::SetSubredditIcon(const std::string& subredditName, const std::string& iconPath)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        m_SubredditIcons[subredditName] = iconPath;
        Save();
    }

    void SubredditIconCache::Load()
    {
        std::ifstream ifstream;
        ifstream.open(GetFilePath());
        if (ifstream.is_open())
        {
            std::string lineValue;
            std::stringstream stringValue;
            while (!ifstream.eof())
            {
                ifstream >> lineValue;
                stringValue << lineValue;
            }

            Json::Value saveData = util::Helpers::GetJsonValueFromString(stringValue.str());
            for (const std::string& member : saveData.getMemberNames())
            {
                m_SubredditIcons[member] = saveData[member].asString();
            }
        }
    }

    void SubredditIconCache::Save()
    {
        std::ofstream ofstream;
        ofstream.open(GetFilePath());

        Json::StreamWriterBuilder builder;
        Json::StreamWriter* writer = builder.newStreamWriter();

        Json::Value saveData;
        for (const auto& [key, value] : m_SubredditIcons)
        {
            saveData[key] = value;
        }
        writer->write(saveData, &ofstream);
        ofstream.close();
    }

    std::string SubredditIconCache::GetFilePath()
    {
        std::stringstream ss;
        ss << g_get_user_config_dir() << "/headlines/sub_icon_cache.json";
        return ss.str();
    }
}