#include "Subreddit.h"
#include "SubredditIconCache.h"
#include <utility>

namespace api
{
    Subreddit::Subreddit(Json::Value jsonData)
    {
        UpdateDataFromJson(std::move(jsonData));
    }

    void Subreddit::UpdateDataFromJson(Json::Value jsonValue)
    {
        m_Url = jsonValue["url"].asString();
        m_Name = jsonValue["name"].asString();
        m_DisplayName = jsonValue["display_name"].asString();
        m_Title = jsonValue["display_name_prefixed"].asString();
        m_PublicDesc = jsonValue["public_description"].asString();
        m_Description = jsonValue["description_html"].asString();
        m_Subscribers = jsonValue["subscribers"].asInt();
        m_IconPath = jsonValue["community_icon"].asString();
        if (!SubredditIconCache::Get()->IsIconCached(m_DisplayName))
        {
            SubredditIconCache::Get()->SetSubredditIcon(m_DisplayName, m_IconPath);
        }

        m_BannerPath = jsonValue["banner_background_image"].asString();
        auto it = std::find(m_BannerPath.begin(), m_BannerPath.end(), '?');
        if (it != m_BannerPath.end())
        {
            m_BannerPath.erase(it, m_BannerPath.end());
        }
        m_CreatedTimestamp = jsonValue["created_utc"].asUInt64();
        m_UserIsSubscriber = jsonValue["user_is_subscriber"].asBool();
    }

    void Subreddit::SetUserIsSubscriber(bool subscriber)
    {
        m_UserIsSubscriber = subscriber;
    }
}
