#include "MultiReddit.h"

namespace api
{
    MultiReddit::MultiReddit(Json::Value jsonData)
    {
        UpdateDataFromJson(jsonData);
    }

    void MultiReddit::SetUserIsSubscriber(bool userIsSubscriber)
    {
        m_Subscriber = userIsSubscriber;
    }

    void MultiReddit::AddSubreddit(const std::string& subreddit)
    {
        m_Subreddits.push_back(subreddit);
    }

    void MultiReddit::RemoveSubreddit(const std::string& subreddit)
    {
        m_Subreddits.erase(std::remove(m_Subreddits.begin(), m_Subreddits.end(), subreddit), m_Subreddits.end());
    }

    void MultiReddit::UpdateDataFromJson(Json::Value jsonValue)
    {
        m_CanEdit = jsonValue["can_edit"].asBool();
        m_DisplayName = jsonValue["display_name"].asString();
        m_Name = jsonValue["name"].asString();
        m_Description = jsonValue["description"].asString();
        m_DescriptionHtml = jsonValue["description_html"].asString();
        m_NumSubs = jsonValue["num_subscribers"].asInt();
        m_CopiedFrom = jsonValue["copied_from"].asString();
        m_IconUrl = jsonValue["icon_url"].asString();

        Json::Value subreddits = jsonValue["subreddits"];
        for (unsigned int i = 0; i < subreddits.size(); ++i)
        {
            m_Subreddits.push_back(subreddits[i]["name"].asString());
        }

        m_CreatedUTC = jsonValue["created_utc"].asUInt64();
        m_Created = jsonValue["created"].asUInt64();
        m_Visibility = jsonValue["visibility"].asString() == "private" ? Visibility::Private : Visibility::Public;
        m_Over18 = jsonValue["over_18"].asBool();
        m_Path = jsonValue["path"].asString();
        m_Owner = jsonValue["owner"].asString();
        m_Subscriber = jsonValue["is_subscriber"].asBool();
        m_OwnerID = jsonValue["owner_id"].asString();
        m_Favourited = jsonValue["is_favorited"].asBool();
    }

    void MultiReddit::SetFromMultiReddit(const MultiRedditRef& multiReddit)
    {
        m_CanEdit = multiReddit->m_CanEdit;
        m_DisplayName = multiReddit->m_DisplayName;
        m_Name = multiReddit->m_Name;
        m_Description = multiReddit->m_Description;
        m_DescriptionHtml = multiReddit->m_DescriptionHtml;
        m_NumSubs = multiReddit->m_NumSubs;
        m_CopiedFrom = multiReddit->m_CopiedFrom;
        m_IconUrl = multiReddit->m_IconUrl;
        m_Subreddits =  multiReddit->m_Subreddits;
        m_CreatedUTC = multiReddit->m_CreatedUTC;
        m_Created = multiReddit->m_Created;
        m_Visibility = multiReddit->m_Visibility;
        m_Over18 = multiReddit->m_Over18;
        m_Path = multiReddit->m_Path;
        m_Owner = multiReddit->m_Owner;
        m_Subscriber = multiReddit->m_Subscriber;
        m_OwnerID = multiReddit->m_OwnerID;
        m_Favourited = multiReddit->m_Favourited;
    }
}