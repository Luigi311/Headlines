#include <sstream>
#include <util/WebSocket.h>
#include <AppSettings.h>
#include "RedditAPI.h"
#include "util/Helpers.h"
#include "util/HttpClient.h"
#include "Application.h"
#include "Subreddit.h"
#include "UI/ListViews/RedditContentListView.h"
#include "Post.h"
#include "UI/Pages/UserPage.h"
#include "User.h"
#include "UI/Widgets/HeaderBar.h"
#include "util/SimpleThread.h"
#include "util/PasswordManager.h"
#include "API/Flair.h"
#include "util/Signals.h"
#include "API/MultiReddit.h"

namespace api
{
    RedditAPI* api::RedditAPI::s_Instance = nullptr;

    RedditAPI* api::RedditAPI::Get()
    {
        if (!s_Instance)
        {
            s_Instance = new RedditAPI();
        }

        return s_Instance;
    }

    void api::RedditAPI::Destroy()
    {
        s_Instance->m_Server.Stop();
        delete s_Instance;
        s_Instance = nullptr;
    }

    api::RedditAPI::RedditAPI()
              : m_Server()
              , m_Token()
              , m_RefreshToken()
              , m_TokenExpiry()
              , m_LoggedIn(false)
              , m_CurrentUser(nullptr)
              , m_GetCurrentUserThread(nullptr)
    {
        LoadToken();

        m_LoginLogoutMainThreadDispatcher.connect([this]()
        {
            util::Signals::Get()->LoginStatusChanged.Fire(m_LoggedIn);
            Application::Get()->GetSidebar()->UpdateMenuItems();
        });
    }

    void api::RedditAPI::Login()
    {
        if (m_LoggedIn)
        {
            return;
        }

        std::string s_AuthUrl = "https://www.reddit.com/api/v1/authorize.compact?client_id=KqO7FfIYaFQMQw&response_type=code&state=lolcats&redirect_uri=http://127.0.0.1:8889&duration=permanent&scope=identity,edit,flair,history,modconfig,modflair,modlog,modposts,modwiki,mysubreddits,privatemessages,read,report,save,submit,subscribe,vote,wikiedit,wikiread";

        util::Helpers::OpenBrowser(s_AuthUrl);
        m_Server.Run(8889);

        m_Server.SetOnArgCallback([this](const std::string& key, const std::string& value)
        {
            if (key == "code")
            {
                util::HttpClient client;
                std::ostringstream oss;
                oss << "grant_type=authorization_code&code=" << value << "&redirect_uri=http://127.0.0.1:8889";
                std::string response = client.Post("https://www.reddit.com/api/v1/access_token", oss.str(), { "Content-Type: application/x-www-form-urlencoded" });

                Json::Value jsonResponse = util::Helpers::GetJsonValueFromString(response);
                m_Token = jsonResponse["access_token"].asString();
                m_RefreshToken = jsonResponse["refresh_token"].asString();
                m_LoggedIn = true;
                m_TokenExpiry = util::Helpers::GetSecondsSinceEpoch() + jsonResponse["expires_in"].asInt64();

                m_LoginLogoutMainThreadDispatcher.emit();

                SaveToken();
                GetUserInfo();
            }
        });
    }

    Json::Value api::RedditAPI::ApiGet(const std::string& path, bool isApiCall, const std::string& args)
    {
        if (IsTokenExpired())
        {
            RefreshToken();
        }

        util::HttpClient client;

        std::string response;
        if (m_LoggedIn && !isApiCall)
        {
            std::stringstream headersStream;
            headersStream << "Authorization: bearer " << m_Token;

            std::stringstream urlStream;
            urlStream << "https://oauth.reddit.com/" << path;
            urlStream << "/.json?" << args;

            response = client.Get(urlStream.str(), { headersStream.str() });
        }
        else
        {
            std::ostringstream urlStream;
            urlStream << "https://api.reddit.com/" << path;
            urlStream << "?" << args;

            response = client.Get(urlStream.str(), {});
        }

        return util::Helpers::GetJsonValueFromString(response);
    }

    Json::Value api::RedditAPI::ApiPost(const std::string& path, const std::string& args)
    {
        if (IsTokenExpired())
        {
            RefreshToken();
        }

        util::HttpClient client;
        std::string response;
        if (m_LoggedIn)
        {
            std::stringstream headersStream;
            headersStream << "Authorization: bearer " << m_Token;

            std::stringstream urlStream;
            urlStream << "https://oauth.reddit.com/" << path;

            response = client.Post(urlStream.str(), args, { headersStream.str() });
        }
        else
        {
            std::ostringstream urlStream;
            urlStream << "https://www.reddit.com/" << path;

            response = client.Post(urlStream.str(), args, {});
        }

        return util::Helpers::GetJsonValueFromString(response);
    }

    Json::Value RedditAPI::ApiPut(const std::string& path, Json::Value args)
    {
        if (IsTokenExpired())
        {
            RefreshToken();
        }

        util::HttpClient client;
        std::string response;
        if (m_LoggedIn)
        {
            std::stringstream headersStream;
            headersStream << "Authorization: bearer " << m_Token;

            std::stringstream urlStream;
            urlStream << "https://oauth.reddit.com/" << path;

            response = client.Put(urlStream.str(), args, { headersStream.str() });
        }
        else
        {
            std::ostringstream urlStream;
            urlStream << "https://www.reddit.com/" << path;

            response = client.Put(urlStream.str(), args, {});
        }

        return util::Helpers::GetJsonValueFromString(response);
    }

    Json::Value RedditAPI::ApiDelete(const std::string& path)
    {
        if (IsTokenExpired())
        {
            RefreshToken();
        }

        util::HttpClient client;
        std::string response;
        if (m_LoggedIn)
        {
            std::stringstream headersStream;
            headersStream << "Authorization: bearer " << m_Token;

            std::stringstream urlStream;
            urlStream << "https://oauth.reddit.com/" << path;

            response = client.Delete(urlStream.str(), { headersStream.str() });
        }
        else
        {
            std::ostringstream urlStream;
            urlStream << "https://www.reddit.com/" << path;

            response = client.Delete(urlStream.str(), {});
        }

        return util::Helpers::GetJsonValueFromString(response);
    }

    void api::RedditAPI::RefreshToken()
    {
        util::HttpClient client;

        std::ostringstream oss;
        oss << "client_id=KqO7FfIYaFQMQw&response_type=code&state=lolcats&redirect_uri=http://127.0.0.1:8889&duration=permanent&scope=identity,edit,flair,history,modconfig,modflair,modlog,modposts,modwiki,mysubreddits,privatemessages,read,report,save,submit,subscribe,vote,wikiedit,wikiread&grant_type=refresh_token&refresh_token=" << m_RefreshToken;

        std::string response = client.Post("https://www.reddit.com/api/v1/access_token", oss.str(), { "Content-Type: application/x-www-form-urlencoded" });

        Json::Value jsonValue = util::Helpers::GetJsonValueFromString(response);

        m_Token = jsonValue["access_token"].asString();
        m_TokenExpiry = util::Helpers::GetSecondsSinceEpoch() + jsonValue["expires_in"].asInt64();
    }

    bool api::RedditAPI::IsTokenExpired() const
    {
        return m_TokenExpiry > 0 && util::Helpers::GetSecondsSinceEpoch() > m_TokenExpiry;
    }

    void api::RedditAPI::SaveToken()
    {
        if (AppSettings::Get()->UseLibSecret())
        {
            util::PasswordManager::SaveToken(m_Token);
            util::PasswordManager::SaveRefreshToken(m_RefreshToken);
            util::PasswordManager::SaveTokenExpiry(m_TokenExpiry);
        }
        else
        {
            std::ofstream ofstream;
            ofstream.open("save.json");

            Json::Value saveData;
            saveData["token"] = m_Token;
            saveData["refresh_token"] = m_RefreshToken;
            saveData["expires"] = (Json::UInt64)m_TokenExpiry;

            Json::StreamWriterBuilder builder;
            Json::StreamWriter* writer = builder.newStreamWriter();
            writer->write(saveData, &ofstream);
            ofstream.close();
        }
    }

    void api::RedditAPI::LoadToken()
    {
        if (AppSettings::Get()->UseLibSecret())
        {
            m_Token = util::PasswordManager::LoadToken();
            if (!m_Token.empty())
            {
                m_RefreshToken = util::PasswordManager::LoadRefreshToken();
                m_TokenExpiry = util::PasswordManager::LoadTokenExpiry();
                m_LoggedIn = !m_RefreshToken.empty();
            }

            if (m_LoggedIn)
            {
                GetUserInfo();
            }
        }
        else
        {
            std::ifstream ifstream;
            ifstream.open("save.json");
            if (ifstream.is_open())
            {
                std::string lineValue;
                std::stringstream stringValue;
                while (!ifstream.eof())
                {
                    ifstream >> lineValue;
                    stringValue << lineValue;
                }

                Json::Value jsonValue = util::Helpers::GetJsonValueFromString(stringValue.str());
                m_Token = jsonValue["token"].asString();
                m_RefreshToken = jsonValue["refresh_token"].asString();
                m_LoggedIn = !m_RefreshToken.empty();
                m_TokenExpiry = jsonValue["expires"].asInt64();

                if (m_LoggedIn)
                {
                    GetUserInfo();
                }
            }
        }
    }

    std::vector<PostRef> api::RedditAPI::GetPosts(PostsType type, int limit, std::string& after)
    {
        std::string requestUrl;
        std::stringstream args_ss;
        std::string requestArgs;

        if (after.empty())
        {
            args_ss << "limit=" << limit;
            requestArgs = args_ss.str();
        }
        else
        {
            args_ss << "limit=" << limit << "&after=" << after;
        }

        switch (type)
        {
            case PostsType::Best:
            {
                requestUrl = "best";
                break;
            }
            case PostsType::Controversial:
            {
                requestUrl = "controversial";
                break;
            }
            case PostsType::Hot:
            {
                requestUrl = "hot";
                break;
            }
            case PostsType::New:
            {
                requestUrl = "new";
                break;
            }
            case PostsType::Random:
            {
                requestUrl = "random";
                break;
            }
            case PostsType::Rising:
            {
                requestUrl = "rising";
                break;
            }
            case PostsType::TopAllTime:
            {
                requestUrl = "top";
                args_ss << "&t=all";
                break;
            }
            case PostsType::TopThisYear:
            {
                requestUrl = "top";
                args_ss << "&t=year";
                break;
            }
            case PostsType::TopThisMonth:
            {
                requestUrl = "top";
                args_ss << "&t=month";
                break;
            }
            case PostsType::TopThisWeek:
            {
                requestUrl = "top";
                args_ss << "&t=week";
                break;
            }
            case PostsType::TopToday:
            {
                requestUrl = "top";
                args_ss << "&t=day";
                break;
            }
            case PostsType::TopNow:
            {
                requestUrl = "top";
                args_ss << "&t=hour";
                break;
            }
        }

        requestArgs = args_ss.str();
        return GetPostsInternal(requestUrl, requestArgs, after);
    }

    std::vector<PostRef> api::RedditAPI::GetSearchPosts(const std::string& searchString, int limit, std::string& after)
    {
        std::string requestUrl = "search";
        std::string requestArgs;
        if (after.empty())
        {
            std::ostringstream oss;
            oss << "q=" << searchString << "&limit=" << limit;
            requestArgs = oss.str();
        }
        else
        {
            std::ostringstream oss;
            oss << "q=" << searchString << "&limit=" << limit << "&after=" << after;
            requestArgs = oss.str();
        }

        return GetPostsInternal(requestUrl, requestArgs, after);
    }

    std::vector<PostRef> api::RedditAPI::GetPostsInternal(const std::string& url, const std::string& args, std::string& after)
    {
        Json::Value response = ApiGet(url, false, args);
        after = response["data"]["after"].asString();

        g_info("RedditAPI::GetPostsInternal - response timestamp: %s", after.c_str());

        Json::Value children = response["data"]["children"];
        unsigned int numResults = children.size();

        g_info("RedditAPI::GetPostsInternal - num posts according to jsoncpp: %u", numResults);
        g_info("RedditAPI::GetPostsInternal - num posts according to reddit: %d", response["data"]["dist"].asInt());

        std::vector<PostRef> posts;
        for (unsigned int i = 0; i < numResults; ++i)
        {
            PostRef pendingPosts = std::make_shared<Post>(children[i]["data"]);
            posts.push_back(pendingPosts);
        }

        return posts;
    }

    PostRef RedditAPI::GetPostFromCommentsUrl(const std::string& url)
    {
        std::stringstream args_ss;
        args_ss << "&limit=" << 0;
        Json::Value jsonValue = ApiGet(url, false, args_ss.str());

        return std::make_shared<Post>(jsonValue[0]["data"]["children"][0]["data"]);
    }

    api::RedditAPI::Comments api::RedditAPI::GetComments(CommentsType type, const std::string& permaLink, int limit, int context, int depth)
    {
        Comments comments;

        std::stringstream args_ss;
        switch (type)
        {
            case CommentsType::Confidence:
            {
                args_ss << "sort=confidence";
                break;
            }
            case CommentsType::Top:
            {
                args_ss << "sort=top";
                break;
            }
            case CommentsType::New:
            {
                args_ss << "sort=new";
                break;
            }
            case CommentsType::Controversial:
            {
                args_ss << "sort=controversial";
                break;
            }
            case CommentsType::Old:
            {
                args_ss << "sort=old";
                break;
            }
            case CommentsType::Random:
            {
                args_ss << "sort=random";
                break;
            }
            case CommentsType::Qa:
            {
                args_ss << "sort=qa";
                break;
            }
            case CommentsType::Live:
            {
                args_ss << "sort=live";
                break;
            }
        }

        args_ss << "&limit=" << limit;
        if (context != -1)
        {
            args_ss << "&context=" << context;
        }
        if (depth != -1)
        {
            args_ss << "&depth=" << depth;
        }

        Json::Value jsonValue = ApiGet(permaLink, false, args_ss.str());
        if (jsonValue.isArray())
        {
            auto commentsArray = jsonValue[1]["data"]["children"];
            for (unsigned int i = 0; i < commentsArray.size(); ++i)
            {
                auto commentData = commentsArray[i]["data"];
                std::string kind = commentsArray[i]["kind"].asString();
                if (kind == "more")
                {
                    auto commentsToLoadArray = commentData["children"];
                    for (unsigned int j = 0; j < commentsToLoadArray.size(); ++j)
                    {
                        comments.m_CommentsToLoad.push_back(commentsToLoadArray[j].asString());
                    }
                }
                else
                {
                    CommentRef comment = std::make_shared<api::Comment>(commentData, kind);

                    if (comment->IsValid())
                    {
                        comments.m_Comments.push_back(comment);
                    }
                }
            }
        }

        return comments;
    }

    api::RedditAPI::Comments api::RedditAPI::GetUserComments(const UserRef& user, CommentsType type, int limit, std::string& after)
    {
        Comments comments;

        std::stringstream ss;
        ss << user->GetUrl() << "comments";

        std::stringstream args_ss;
        switch (type)
        {
            case CommentsType::Old:
            case CommentsType::Random:
            case CommentsType::Qa:
            case CommentsType::Live:
            case CommentsType::Confidence:
            {
                ASSERT(false, "api::RedditAPI::GetUserComments invalid CommentsType: %i", (int)type);
                args_ss << "sort=new";
                break;
            }
            case CommentsType::Top:
            {
                args_ss << "sort=top";
                break;
            }
            case CommentsType::New:
            {
                args_ss << "sort=new";
                break;
            }
            case CommentsType::Controversial:
            {
                args_ss << "sort=controversial";
                break;
            }
        }

        args_ss << "&limit=" << limit;
        if (!after.empty())
        {
            args_ss << "&after=" << after;
        }

        Json::Value jsonValue = ApiGet(ss.str(), false, args_ss.str());
        after = jsonValue["data"]["after"].asString();

        auto commentsArray = jsonValue["data"]["children"];
        for (unsigned int i = 0; i < commentsArray.size(); ++i)
        {
            auto commentData = commentsArray[i]["data"];
            std::string kind = commentsArray[i]["kind"].asString();
            if (kind == "more")
            {
                auto commentsToLoadArray = commentData["children"];
                for (unsigned int j = 0; j < commentsToLoadArray.size(); ++j)
                {
                    comments.m_CommentsToLoad.push_back(commentsToLoadArray[j].asString());
                }
            }
            else
            {
                CommentRef comment = std::make_shared<api::Comment>(commentData, kind);

                if (comment->IsValid())
                {
                    comments.m_Comments.push_back(comment);
                }
            }
        }

        return comments;
    }

    api::RedditAPI::Comments RedditAPI::GetUserSavedComments(int limit, std::string& after)
    {
        Comments comments;

        std::stringstream ss;
        ss << GetCurrentUser()->GetUrl() << "saved";

        std::stringstream args_ss;
        args_ss << "type=comments&limit=" << limit;
        if (!after.empty())
        {
            args_ss << "&after=" << after;
        }

        Json::Value jsonValue = ApiGet(ss.str(), false, args_ss.str());
        after = jsonValue["data"]["after"].asString();

        auto commentsArray = jsonValue["data"]["children"];
        for (unsigned int i = 0; i < commentsArray.size(); ++i)
        {
            auto commentData = commentsArray[i]["data"];
            std::string kind = commentsArray[i]["kind"].asString();
            if (kind == "more")
            {
                auto commentsToLoadArray = commentData["children"];
                for (unsigned int j = 0; j < commentsToLoadArray.size(); ++j)
                {
                    comments.m_CommentsToLoad.push_back(commentsToLoadArray[j].asString());
                }
            }
            else
            {
                CommentRef comment = std::make_shared<api::Comment>(commentData, kind);

                if (comment->IsValid())
                {
                    comments.m_Comments.push_back(comment);
                }
            }
        }

        return comments;
    }

    std::vector<CommentRef> api::RedditAPI::GetMoreComments(const std::string& linkId, int depth, const std::string& parentID, std::vector<std::string> commentIDs)
    {
        std::vector<CommentRef> ret;

        std::stringstream ss;
        ss << "api_type=json&depth=" << depth << "&link_id=" << linkId << "&children=";
        for (unsigned int i = 0; i < commentIDs.size(); ++i)
        {
            ss << commentIDs[i];
            if (i < commentIDs.size() - 1)
            {
                ss << ",";
            }
        }

        Json::Value moreComments = ApiGet("api/morechildren", true, ss.str());

        //keep a map of comments for convenience because this api call returns a flat structure :(
        std::unordered_map<std::string, CommentRef> newComments;

        auto commentsArray = moreComments["json"]["data"]["things"];
        for (unsigned int i = 0; i < commentsArray.size(); ++i)
        {
            if (i == 0)
            {
                //first comment is the current comment
                continue;
            }

            auto commentData = commentsArray[i]["data"];

            CommentRef comment = std::make_shared<api::Comment>(commentData, commentsArray[i]["kind"].asString());
            newComments[comment->GetFullID()] = comment;

            std::string commentParentId = commentData["parent_id"].asString();
            if (parentID == commentParentId)
            {
                ret.push_back(comment);
            }
            else if (newComments.find(commentParentId) != newComments.end())
            {
                newComments[commentParentId]->AddChild(comment);
            }
        }

        return ret;
    }

    std::vector<SubredditRef> api::RedditAPI::GetSearchSubreddits(const std::string& searchString, int limit, std::string& after)
    {
        std::string requestUrl = "subreddits/search";
        std::string requestArgs;
        if (after.empty())
        {
            std::ostringstream oss;
            oss << "q=" << searchString << "&limit=" << limit;
            requestArgs = oss.str();
        }
        else
        {
            std::ostringstream oss;
            oss << "q=" << searchString << "&limit=" << limit << "&after=" << after;
            requestArgs = oss.str();
        }

        Json::Value response = ApiGet(requestUrl, false, requestArgs);
        after = response["data"]["after"].asString();

        unsigned int numResults = response["data"]["children"].size();

        std::vector<SubredditRef> subreddits;
        for (unsigned int i = 0; i < numResults; ++i)
        {
            SubredditRef newSubreddit = std::make_shared<Subreddit>(response["data"]["children"][i]["data"]);
            subreddits.push_back(newSubreddit);
        }

        return subreddits;
    }

    std::vector<PostRef> api::RedditAPI::GetSubredditPosts(const SubredditRef& subreddit, PostsType type, int limit, std::string& after)
    {
        std::string requestArgs;
        std::stringstream args_ss;
        if (after.empty())
        {
            args_ss << "limit=" << limit;
        }
        else
        {
            args_ss << "limit=" << limit << "&after=" << after;
        }

        std::stringstream ss;
        ss << subreddit->GetUrl();
        switch (type)
        {
            case PostsType::Best:
            {
                ss << "best";
                break;
            }
            case PostsType::Controversial:
            {
                ss << "controversial";
                break;
            }
            case PostsType::Hot:
            {
                ss << "hot";
                break;
            }
            case PostsType::New:
            {
                ss << "new";
                break;
            }
            case PostsType::Random:
            {
                ss << "random";
                break;
            }
            case PostsType::Rising:
            {
                ss << "rising";
                break;
            }
            case PostsType::TopAllTime:
            {
                ss << "top";
                args_ss << "&t=all";
                break;
            }
            case PostsType::TopThisYear:
            {
                ss << "top";
                args_ss << "&t=year";
                break;
            }
            case PostsType::TopThisMonth:
            {
                ss << "top";
                args_ss << "&t=month";
                break;
            }
            case PostsType::TopThisWeek:
            {
                ss << "top";
                args_ss << "&t=week";
                break;
            }
            case PostsType::TopToday:
            {
                ss << "top";
                args_ss << "&t=day";
                break;
            }
            case PostsType::TopNow:
            {
                ss << "top";
                args_ss << "&t=hour";
                break;
            }
        }

        requestArgs = args_ss.str();
        return GetPostsInternal(ss.str(), requestArgs, after);
    }

    std::vector<UserRef> api::RedditAPI::GetSearchUsers(const std::string& searchString, int limit, std::string& after)
    {
        std::string requestUrl = "users/search";
        std::string requestArgs;
        if (after.empty())
        {
            std::ostringstream oss;
            oss << "q=" << searchString << "&limit=" << limit;
            requestArgs = oss.str();
        }
        else
        {
            std::ostringstream oss;
            oss << "q=" << searchString << "&limit=" << limit << "&after=" << after;
            requestArgs = oss.str();
        }

        Json::Value response = ApiGet(requestUrl, false, requestArgs);
        after = response["data"]["after"].asString();

        unsigned int numResults = response["data"]["children"].size();

        std::vector<UserRef> users;
        for (unsigned int i = 0; i < numResults; ++i)
        {
            UserRef newSubreddit = std::make_shared<User>(response["data"]["children"][i]["data"]);
            users.push_back(newSubreddit);
        }

        return users;
    }

    std::vector<PostRef> api::RedditAPI::GetUserPosts(const UserRef& user, PostsType type, int limit, std::string& after)
    {
        std::stringstream ss;
        ss << user->GetUrl() << "submitted";

        std::string requestArgs;
        std::stringstream args_ss;

        switch (type)
        {
            case PostsType::Best:
            case PostsType::Random:
            case PostsType::Rising:
            {
                g_warning("api::RedditAPI::GetUserPosts - Unsupported PostsType");
                args_ss << "sort=new";
                break;
            }
            case PostsType::Controversial:
            {
                args_ss << "sort=controversial";
                break;
            }
            case PostsType::Hot:
            {
                args_ss << "sort=hot";
                break;
            }
            case PostsType::New:
            {
                args_ss << "sort=new";
                break;
            }
            case PostsType::TopAllTime:
            {
                args_ss << "sort=top&t=all";
                break;
            }
            case PostsType::TopThisYear:
            {
                args_ss << "sort=top&t=year";
                break;
            }
            case PostsType::TopThisMonth:
            {
                args_ss << "sort=top&t=month";
                break;
            }
            case PostsType::TopThisWeek:
            {
                args_ss << "sort=top&t=week";
                break;
            }
            case PostsType::TopToday:
            {
                args_ss << "sort=top&t=day";
                break;
            }
            case PostsType::TopNow:
            {
                args_ss << "sort=top&t=hour";
                break;
            }
        }

        if (after.empty())
        {
            args_ss << "&limit=" << limit;
        }
        else
        {
            args_ss << "&limit=" << limit << "&after=" << after;
        }

        return GetPostsInternal(ss.str(), args_ss.str(), after);
    }

    std::vector<PostRef> RedditAPI::GetMultiRedditPosts(const MultiRedditRef& multiReddit, PostsType type, int limit, std::string& after)
    {
        std::stringstream ss;
        ss << multiReddit->GetPath();

        std::stringstream args_ss;
        switch (type)
        {
            case PostsType::Best:
            case PostsType::Random:
            case PostsType::Controversial:
            {
                g_warning("api::RedditAPI::GetMultiRedditPosts - Unsupported PostsType");
                ss << "/new";
                break;
            }
            case PostsType::Rising:
            {
                ss << "/rising";
                break;
            }
            case PostsType::Hot:
            {
                ss << "/hot";
                break;
            }
            case PostsType::New:
            {
                ss << "/new";
                break;
            }
            case PostsType::TopAllTime:
            {
                ss << "/top";
                args_ss << "t=all";
                break;
            }
            case PostsType::TopThisYear:
            {
                ss << "/top";
                args_ss << "t=year";
                break;
            }
            case PostsType::TopThisMonth:
            {
                ss << "/top";
                args_ss << "t=month";
                break;
            }
            case PostsType::TopThisWeek:
            {
                ss << "/top";
                args_ss << "t=week";
                break;
            }
            case PostsType::TopToday:
            {
                ss << "/top";
                args_ss << "t=day";
                break;
            }
            case PostsType::TopNow:
            {
                ss << "/top";
                args_ss << "t=hour";
                break;
            }
        }

        if (after.empty())
        {
            args_ss << "&limit=" << limit;
        }
        else
        {
            args_ss << "&limit=" << limit << "&after=" << after;
        }

        return GetPostsInternal(ss.str(), args_ss.str(), after);
    }

    std::vector<PostRef> RedditAPI::GetUserUpvotedPosts(int limit, std::string& after)
    {
        std::string requestArgs;
        std::stringstream ss;
        ss << GetCurrentUser()->GetUrl() << "upvoted";

        std::stringstream args_ss;
        args_ss << "&limit=" << limit;
        if (!after.empty())
        {
            args_ss << "&after=" << after;
        }

        return GetPostsInternal(ss.str(), args_ss.str(), after);
    }

    std::vector<PostRef> RedditAPI::GetUserDownvotedPosts(int limit, std::string& after)
    {
        std::string requestArgs;

        std::stringstream ss;
        ss << GetCurrentUser()->GetUrl() << "downvoted";

        std::stringstream args_ss;
        args_ss << "&limit=" << limit;
        if (!after.empty())
        {
            args_ss << "&after=" << after;
        }

        return GetPostsInternal(ss.str(), args_ss.str(), after);
    }

    std::vector<PostRef> RedditAPI::GetUserGildedPosts(int limit, std::string& after)
    {
        std::string requestArgs;
        std::stringstream ss;
        ss << GetCurrentUser()->GetUrl() << "gilded";

        std::stringstream args_ss;
        args_ss << "limit=" << limit;
        if (!after.empty())
        {
            args_ss << "&after=" << after;
        }

        return GetPostsInternal(ss.str(), args_ss.str(), after);
    }


    std::vector<PostRef> RedditAPI::GetUserHiddenPosts(int limit, std::string& after)
    {
        std::string requestArgs;

        std::stringstream ss;
        ss << GetCurrentUser()->GetUrl() << "hidden";

        std::stringstream args_ss;
        args_ss << "&limit=" << limit;
        if (!after.empty())
        {
            args_ss << "&after=" << after;
        }

        return GetPostsInternal(ss.str(), args_ss.str(), after);
    }

    std::vector<PostRef> api::RedditAPI::GetUserSavedPosts(int limit, std::string& after)
    {
        std::string requestArgs;
        std::stringstream ss;
        ss << GetCurrentUser()->GetUrl() << "saved";

        std::stringstream args_ss;
        args_ss << "&limit=" << limit;
        if (!after.empty())
        {
            args_ss << "&after=" << after;
        }

        return GetPostsInternal(ss.str(), args_ss.str(), after);
    }

    void api::RedditAPI::Logout()
    {
        m_LoggedIn = false;
        m_Token = "";
        m_RefreshToken = "";
        m_TokenExpiry = 0;
        SaveToken();
        m_LoginLogoutMainThreadDispatcher.emit();
    }

    void api::RedditAPI::GetUserInfo()
    {
        m_GetCurrentUserThread = new util::SimpleThread([this](util::ThreadWorker*)
        {
            Json::Value meData = ApiGet("api/v1/me", false, "");

            m_DisplayName = meData["subreddit"]["display_name_prefixed"].asString();
            m_UserUrl = meData["subreddit"]["url"].asString();
            if (!m_CurrentUser || m_CurrentUser->GetUrl().empty())
            {
                std::stringstream url_ss;
                url_ss << m_UserUrl << "about";
                std::string requestUrl = url_ss.str();

                Json::Value response = ApiGet(requestUrl, false, "");

                m_CurrentUser = std::make_shared<User>(response["data"]);
            }
            m_GetCurrentUserThread = nullptr;
        });
    }

    UserRef api::RedditAPI::GetCurrentUser()
    {
        while (m_GetCurrentUserThread)
        {
            util::Helpers::MSleep(100);
        }

        return m_CurrentUser;
    }

    void api::RedditAPI::Vote(const std::string& fullName, int dir)
    {
        std::stringstream argsStream;
        argsStream << "dir=" << dir << "&id=" << fullName;
        Json::Value response = ApiPost("api/vote", argsStream.str());
    }

    void api::RedditAPI::Save(const std::string &fullName)
    {
        std::stringstream argsStream;
        argsStream << "id=" << fullName;
        Json::Value response = ApiPost("api/save", argsStream.str());
    }

    void api::RedditAPI::UnSave(const std::string &fullName)
    {
        std::stringstream argsStream;
        argsStream << "id=" << fullName;
        Json::Value response = ApiPost("api/unsave", argsStream.str());
    }

    Json::Value RedditAPI::Comment(const std::string& fullName, const std::string& text)
    {
        std::stringstream argsStream;
        argsStream << "parent=" << fullName << "&text=" << text << "&return_rtjson=true";
        return ApiPost("api/comment", argsStream.str());
    }

    void RedditAPI::Delete(const std::string& fullName)
    {
        std::stringstream argsStream;
        argsStream << "id=" << fullName;
        Json::Value response = ApiPost("api/del", argsStream.str());
    }

    std::pair<std::string, util::WebSocket*> RedditAPI::UploadMedia(const std::string& fileName)
    {
        std::stringstream ss;
        ss << "filepath=" << fileName << "&mimetype=" << util::Helpers::GetMimeType(fileName);
        Json::Value response = ApiPost("api/media/asset.json", ss.str());

        std::string websocketurl = response["asset"]["websocket_url"].asString();
        util::WebSocket* websocket = new util::WebSocket(websocketurl);

        Json::Value responseArgs = response["args"];
        std::stringstream url_ss;
        url_ss << "https:" << responseArgs["action"].asString();

        std::stringstream args_ss;
        Json::Value argFields = responseArgs["fields"];

        util::HttpClient client;
        std::string uploadResponse = client.UploadMedia(url_ss.str(), argFields, fileName, {  });

        xmlDocPtr xmlResponse = xmlParseMemory(uploadResponse.c_str(), (int)uploadResponse.length());
        xmlNodePtr docRoot = xmlDocGetRootElement(xmlResponse);
        xmlNodePtr node = util::Helpers::FindXmlNodeByName(docRoot, (const xmlChar*)"Location");
        std::string mediaUrl((char*) node->children->content, strlen((char*)node->children->content));
        return std::make_pair(mediaUrl, websocket);
    }

    PostRef RedditAPI::SubmitTextPost(const SubredditRef& subreddit, const std::string& title, const std::string& text, bool nsfw, bool spoiler, const FlairRef& flair, std::string& outError)
    {
        std::stringstream ss;
        ss << "ad=false";
        ss << "&api_type=json";
        ss << "&kind=self";
        ss << "&nsfw=" << (nsfw ? "true" : "false");
        ss << "&resubmit=false";
        ss << "&sendreplies=true"; //TODO make this optional
        ss << "&spoiler=" << (spoiler ? "true" : "false");
        ss << "&sr=" << subreddit->GetDisplayName();
        ss << "&text=" << text;
        ss << "&title=" << title;
        ss << "&validate_on_submit=true";
        if (flair)
        {
           ss << "&flair_text=" << flair->GetText();
           ss << "&flair_id=" << flair->GetID();
        }

        return SubmitPostInternal(ss.str(), outError);
    }

    PostRef RedditAPI::SubmitUrlPost(const SubredditRef& subreddit, const std::string& title, const std::string& url, bool nsfw, bool spoiler, const FlairRef& flair, std::string& outError)
    {
        std::stringstream ss;
        ss << "ad=false";
        ss << "&api_type=json";
        ss << "&kind=link";
        ss << "&nsfw=" << (nsfw ? "true" : "false");
        ss << "&resubmit=false";
        ss << "&sendreplies=true"; //TODO make this optional
        ss << "&spoiler=" << (spoiler ? "true" : "false");
        ss << "&sr=" << subreddit->GetDisplayName();
        ss << "&url=" << url;
        ss << "&title=" << title;
        ss << "&validate_on_submit=true";
        if (flair)
        {
            ss << "&flair_text=" << flair->GetText();
            ss << "&flair_id=" << flair->GetID();
        }

        return SubmitPostInternal(ss.str(), outError);
    }

    void RedditAPI::SubmitImagePost(const SubredditRef& subreddit, const std::string& title, const std::string& url, util::WebSocket* webSocket, bool nsfw, bool spoiler, const FlairRef& flair, const std::function<void(PostRef)>& onPosted, std::string& outError)
    {
        std::stringstream ss;
        ss << "ad=false";
        ss << "&api_type=json";
        ss << "&kind=image";
        ss << "&nsfw=" << (nsfw ? "true" : "false");
        ss << "&resubmit=false";
        ss << "&sendreplies=true"; //TODO make this optional
        ss << "&spoiler=" << (spoiler ? "true" : "false");
        ss << "&sr=" << subreddit->GetDisplayName();
        ss << "&url=" << url;
        ss << "&title=" << title;
        ss << "&validate_on_submit=true";
        if (flair)
        {
            ss << "&flair_text=" << flair->GetText();
            ss << "&flair_id=" << flair->GetID();
        }

        webSocket->SetMessageCallback([this, onPosted](const std::string& msg)
        {
            Json::Value msgJson = util::Helpers::GetJsonValueFromString(msg);
            std::string type = msgJson["type"].asString();
            if (type == "success")
            {
                std::string postUrl = msgJson["payload"]["redirect"].asString();
                postUrl.erase(0, strlen("https://www.reddit.com/"));
                PostRef post = GetPostFromCommentsUrl(postUrl);
                onPosted(post);
            }
            else
            {
                onPosted(PostRef());
            }
        });

        SubmitPostInternal(ss.str(), outError);
    }

    void RedditAPI::SubmitVideoPost(const SubredditRef& subreddit, const std::string& title, const std::string& url, const std::string& thumbnailUrl, util::WebSocket* webSocket, bool nsfw, bool spoiler, const FlairRef& flair, const std::function<void(PostRef)>& onPosted, std::string& outError)
    {
        std::stringstream ss;
        ss << "ad=false";
        ss << "&api_type=json";
        ss << "&kind=video";
        ss << "&nsfw=" << (nsfw ? "true" : "false");
        ss << "&resubmit=false";
        ss << "&sendreplies=true"; //TODO make this optional
        ss << "&spoiler=" << (spoiler ? "true" : "false");
        ss << "&sr=" << subreddit->GetDisplayName();
        ss << "&url=" << url;
        ss << "&video_poster_url=" << thumbnailUrl;
        ss << "&title=" << title;
        ss << "&validate_on_submit=true";
        if (flair)
        {
            ss << "&flair_text=" << flair->GetText();
            ss << "&flair_id=" << flair->GetID();
        }

        webSocket->SetMessageCallback([this, onPosted](const std::string& msg)
        {
            Json::Value msgJson = util::Helpers::GetJsonValueFromString(msg);
            std::string type = msgJson["type"].asString();
            if (type == "success")
            {
                std::string postUrl = msgJson["payload"]["redirect"].asString();
                postUrl.erase(0, strlen("https://www.reddit.com/"));
                PostRef post = GetPostFromCommentsUrl(postUrl);
                onPosted(post);
            }
            else
            {
                onPosted(PostRef());
            }
        });

        SubmitPostInternal(ss.str(), outError);
    }

    PostRef RedditAPI::SubmitPostInternal(const std::string& url, std::string& outError)
    {
        Json::Value response = ApiPost("api/submit", url);

        if(!response["json"]["errors"].isNull() && !response["json"]["errors"][0].isNull() && response["json"]["errors"][0].isArray())
        {
            //second value is the error message
            outError = response["json"]["errors"][0][1].asString();
            return PostRef();
        }
        if (!response["errors"].empty())
        {
            return PostRef();
        }

        std::string postUrl = response["json"]["data"]["url"].asString();
        postUrl.erase(0, strlen("https://www.reddit.com/"));
        return GetPostFromCommentsUrl(postUrl);
    }

    void RedditAPI::Subscribe(const UserRef& user)
    {
        std::stringstream ss;
        ss << "sr_name=" << user->GetDisplayName();
        ss << "&api_type=json";
        ss << "&action=sub";

        ApiPost("api/subscribe", ss.str());
    }

    void RedditAPI::UnSubscribe(const UserRef& user)
    {
        std::stringstream ss;
        ss << "sr_name=" << user->GetDisplayName();
        ss << "&api_type=json";
        ss << "&action=unsub";

        ApiPost("api/subscribe", ss.str());
    }

    void RedditAPI::Subscribe(const SubredditRef& subreddit)
    {
        std::stringstream ss;
        ss << "sr_name=" << subreddit->GetDisplayName();
        ss << "&api_type=json";
        ss << "&action=sub";

        ApiPost("api/subscribe", ss.str());
        m_UserSubreddits.push_back(subreddit);
    }

    void RedditAPI::UnSubscribe(const SubredditRef& subreddit)
    {
        std::stringstream ss;
        ss << "sr_name=" << subreddit->GetDisplayName();
        ss << "&api_type=json";
        ss << "&action=unsub";

        ApiPost("api/subscribe", ss.str());
        m_UserSubreddits.erase(std::remove(m_UserSubreddits.begin(), m_UserSubreddits.end(), subreddit), m_UserSubreddits.end());
    }

    std::vector<SubredditRef> RedditAPI::GetUserSubreddits(int limit, std::string& after, bool clearCache)
    {
        if (m_UserSubreddits.empty() || clearCache)
        {
            std::string requestUrl = "/subreddits/mine/subscriber";
            std::string requestArgs;
            if (after.empty())
            {
                std::ostringstream oss;
                oss << "&limit=" << limit;
                requestArgs = oss.str();
            } else
            {
                std::ostringstream oss;
                oss << "&limit=" << limit << "&after=" << after;
                requestArgs = oss.str();
            }

            Json::Value response = ApiGet(requestUrl, false, requestArgs);
            after = response["data"]["after"].asString();

            unsigned int numResults = response["data"]["children"].size();

            std::vector<SubredditRef> subreddits;
            for (unsigned int i = 0; i < numResults; ++i)
            {
                SubredditRef newSubreddit = std::make_shared<Subreddit>(response["data"]["children"][i]["data"]);
                subreddits.push_back(newSubreddit);
            }

            m_UserSubreddits = subreddits;
        }

        return m_UserSubreddits;
    }

    UserRef RedditAPI::GetUser(const std::string& username)
    {
        std::stringstream url_ss;
        url_ss << "user/" << username << "/about";
        Json::Value response = ApiGet(url_ss.str(), false, {});

        return std::make_shared<User>(response["data"]);
    }

    SubredditRef RedditAPI::GetSubreddit(const std::string& subreddit)
    {
        std::stringstream url_ss;
        url_ss << "r/" << subreddit << "/about";
        Json::Value response = ApiGet(url_ss.str(), false, {});

        return std::make_shared<Subreddit>(response["data"]);
    }

    std::vector<FlairRef> RedditAPI::GetSubredditPostFlairs(const SubredditRef& subreddit)
    {
        std::vector<FlairRef> ret;

        std::stringstream ss;
        ss << subreddit->GetUrl() << "api/link_flair_v2";

        Json::Value response = ApiGet(ss.str(), false);

        if (!response.isArray() && !response["error"].empty())
        {
            if (response["error"].isInt())
            {
                g_warning("RedditAPI::GetSubredditPostFlairs - error %d", response["error"].asInt());
            }
            else
            {
                g_warning("RedditAPI::GetSubredditPostFlairs - error %s", response["error"].asCString());
            }
            return ret;
        }

        for (unsigned int i = 0; i < response.size(); ++i)
        {
            FlairRef flair = std::make_shared<Flair>(response[i]);
            ret.push_back(flair);
        }

        return ret;
    }

    PostRef RedditAPI::GetPost(const std::string& url)
    {
        Json::Value json = ApiGet(url, false);
        return std::make_shared<api::Post>(json[0]["data"]["children"][0]["data"]);
    }

    std::vector<MultiRedditRef> RedditAPI::GetUserMultiReddits()
    {
        Json::Value multiRedditArray = ApiGet("api/multi/mine", false);
        if (multiRedditArray.size() == 0)
        {
            return std::vector<MultiRedditRef>();
        }

        std::vector<MultiRedditRef> ret;
        for (unsigned int i = 0; i < multiRedditArray.size(); ++i)
        {
            ret.push_back(std::make_shared<MultiReddit>(multiRedditArray[i]["data"]));
        }

        return ret;
    }

    void RedditAPI::AddSubredditToMultiReddit(const SubredditRef& subreddit, const MultiRedditRef& multireddit)
    {
        Json::Value model;
        model["name"] = subreddit->GetDisplayName();

        Json::Value modelValue = Json::Value();
        modelValue["name"] = "model";
        modelValue["value"] = model;

        Json::Value multiVaue = Json::Value();
        multiVaue["name"] = "multipath";
        multiVaue["value"] = multireddit->GetPath();

        Json::Value subValue = Json::Value();
        subValue["name"] = "srname";
        subValue["value"] = subreddit->GetDisplayName();

        Json::Value args;
        args[0] = modelValue;
        args[1] = multiVaue;
        args[2] = subValue;

        std::string url = util::Helpers::FormatString("api/multi%s/r/%s", multireddit->GetPath().c_str(), subreddit->GetDisplayName().c_str());
        Json::Value response = ApiPut(url, args);

        if (response["explanation"].isNull())
        {
            Application::Get()->ShowNotification(util::Helpers::FormatString("Added %s to %s.", subreddit->GetDisplayName().c_str(), multireddit->GetDisplayName().c_str()));
        }
        else
        {
            Application::Get()->ShowNotification(util::Helpers::FormatString("Error adding to Multireddit: %s", response["explanation"].asCString()));
        }

        multireddit->AddSubreddit(subreddit->GetDisplayName());
    }

    void RedditAPI::RemoveSubredditFromMultiReddit(const SubredditRef& subreddit, const MultiRedditRef& multireddit)
    {
        std::string url = util::Helpers::FormatString("api/multi%s/r/%s", multireddit->GetPath().c_str(), subreddit->GetDisplayName().c_str());
        Json::Value response = ApiDelete(url);
        multireddit->RemoveSubreddit(subreddit->GetDisplayName());
    }

    void RedditAPI::AddUserToMultiReddit(const UserRef& user, const MultiRedditRef& multireddit)
    {
        Json::Value model;
        model["name"] = user->GetDisplayName().c_str();

        Json::Value modelValue = Json::Value();
        modelValue["name"] = "model";
        modelValue["value"] = model;

        Json::Value multiVaue = Json::Value();
        multiVaue["name"] = "multipath";
        multiVaue["value"] = multireddit->GetPath();

        Json::Value subValue = Json::Value();
        subValue["name"] = "srname";
        subValue["value"] = user->GetDisplayName().c_str();

        Json::Value args;
        args[0] = modelValue;
        args[1] = multiVaue;
        args[2] = subValue;

        std::string url = util::Helpers::FormatString("api/multi%s/r/%s", multireddit->GetPath().c_str(), user->GetDisplayName().c_str());
        Json::Value response = ApiPut(url, args);

        if (response["explanation"].isNull())
        {
            Application::Get()->ShowNotification(util::Helpers::FormatString("Added %s to %s.", user->GetDisplayName().c_str(), multireddit->GetDisplayName().c_str()));
        }
        else
        {
            Application::Get()->ShowNotification(util::Helpers::FormatString("Error adding to Multireddit: %s", response["explanation"].asCString()));
        }
    }

    void RedditAPI::RemoveUserFromMultiReddit(const UserRef& user, const MultiRedditRef& multireddit)
    {
        std::string url = util::Helpers::FormatString("api/multi%s/r/%s", multireddit->GetPath().c_str(), user->GetDisplayName().c_str());
        Json::Value response = ApiDelete(url);
    }

    void RedditAPI::DeleteMultiReddit(const MultiRedditRef& multireddit)
    {
        std::string url = util::Helpers::FormatString("api/multi%s", multireddit->GetPath().c_str());
        Json::Value response = ApiDelete(url);
    }

    api::MultiRedditRef RedditAPI::CreateOrUpdateMultiReddit(const std::string& path,
                                              const std::string& name,
                                              const std::string& desc,
                                              bool isPrivate,
                                              const std::vector<std::string>& subscriptions)
    {
        Json::Value model;
        model["description_md"] = desc.c_str();
        model["display_name"] = name.c_str();
        for (unsigned int i = 0; i < subscriptions.size(); ++i)
        {
            model["subreddits"][i]["name"] = subscriptions[i];
        }
        model["visibility"] = isPrivate ? "private" : "public";

        Json::Value modelValue = Json::Value();
        modelValue["name"] = "model";
        modelValue["value"] = model;

        Json::Value args;
        args[0] = modelValue;

        std::string url = util::Helpers::FormatString("api/multi%s", path.c_str());
        Json::Value response = ApiPut(url, args);

        if (!response["message"].isNull())
        {
            g_warning("RedditAPI::CreateOrUpdateMultiReddit error: %s", response["message"].asCString());
            return MultiRedditRef();
        }

        return std::make_shared<MultiReddit>(response["data"]);
    }
}