#pragma once
#include "RedditContentProvider.h"
#include "API/api_fwd.h"

namespace ui
{
    class PostWidget;
    class SubredditPostsContentProvider : public RedditContentProvider
    {
    public:
        SubredditPostsContentProvider();
        void SetSubreddit(const api::SubredditRef& subreddit);

    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;

        api::SubredditRef m_Subreddit;
    };
}
