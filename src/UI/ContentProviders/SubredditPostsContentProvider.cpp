#include <AppSettings.h>
#include "SubredditPostsContentProvider.h"
#include "UI/Widgets/PostWidget.h"
#include "API/RedditAPI.h"
#include "Application.h"
#include "util/ThreadWorker.h"

namespace ui
{
    SubredditPostsContentProvider::SubredditPostsContentProvider()
    {

    }
    
    std::vector<RedditContentListItemRef> SubredditPostsContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<api::PostRef> posts = api::RedditAPI::Get()->GetSubredditPosts(m_Subreddit, Application::Get()->GetSubredditPostsSorting(), AppSettings::Get()->GetInt("num_posts_to_load", 5), lastTimeStamp);
        std::vector<RedditContentListItemRef> uiElements;
        for (const api::PostRef& post : posts)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            PostWidgetRef postUI = std::make_shared<PostWidget>(post);
            uiElements.push_back(postUI);
        }

        return uiElements;
    }

    void SubredditPostsContentProvider::SetSubreddit(const api::SubredditRef& subreddit)
    {
        m_Subreddit = subreddit;
    }

    std::string SubredditPostsContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No posts found. Retry?";
    }
}