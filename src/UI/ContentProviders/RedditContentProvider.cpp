#include "RedditContentProvider.h"
#include "util/SimpleThread.h"
#include "util/ThreadWorker.h"
#include "API/RedditAPI.h"

namespace ui
{
    void RedditContentProvider::GetContentAsync(std::function<void(std::vector<RedditContentListItemRef>)> onFinsihed, std::function<void()> onFailed)
    {
        if (LoadingContent())
        {
            return;
        }

        if (!m_HasMoreContent)
        {
            return;
        }

        struct ThreadStorage
        {
            std::vector<RedditContentListItemRef> content;
            bool error;
        };
        ThreadStorage* storage = new ThreadStorage();
        std::lock_guard<std::mutex> lock(m_ThreadMutex);
        m_Thread = new util::SimpleThread(
        [this, storage](util::ThreadWorker* worker)
            {
                if (worker->IsCancelled())
                    return;

                //ensure user has been loaded before loading posts.
                api::RedditAPI::Get()->GetCurrentUser();

                storage->content = GetContent(worker, &storage->error);
            },
            [this, onFinsihed, onFailed, storage](bool cancelled, util::SimpleThread*)
            {
                if (!cancelled)
                {
                    if (storage->error)
                    {
                        onFailed();
                    }
                    else
                    {
                        onFinsihed(storage->content);
                    }

                    delete storage;
                }

                std::lock_guard<std::mutex> lock(m_ThreadMutex);
                m_Thread = nullptr;
            });
    }

    std::vector<RedditContentListItemRef> RedditContentProvider::GetContent(util::ThreadWorker* worker, bool* error)
    {
        g_info("RedditContentProvider::GetContent");

        if (worker->IsCancelled())
            return std::vector<RedditContentListItemRef>();

        std::string timestamp = m_LastTimeStamp;
        std::vector<RedditContentListItemRef> newContent = GetContentInternal(timestamp, worker);

        g_info("RedditContentProvider::GetContent - Retrieved %zu items", newContent.size());

        if (worker->IsCancelled())
        {
            g_info("RedditContentProvider::GetContent - Worker Cancelled");
            return std::vector<RedditContentListItemRef>();
        }

        if (!newContent.empty())
        {
            g_info("RedditContentProvider::GetContent - New timestamp %s", timestamp.c_str());
            m_LastTimeStamp = timestamp;
        }

        if (m_HasMoreContent && newContent.empty())
        {
            *error = true;
            g_info("RedditContentProvider::GetContent - Failed to retrieve content that should exist.");
            return std::vector<RedditContentListItemRef>();
        }

        if (worker->IsCancelled())
        {
            g_info("RedditContentProvider::GetContent - Worker Cancelled");
            return std::vector<RedditContentListItemRef>();
        }

        if (m_LastTimeStamp.empty())
        {
            g_info("RedditContentProvider::GetContent - Reached end of listing");
            m_HasMoreContent = false;
        }

        if (worker->IsCancelled())
        {
            g_info("RedditContentProvider::GetContent - Worker Cancelled");
            return std::vector<RedditContentListItemRef>();
        }

        return newContent;
    }

    void RedditContentProvider::Reset()
    {
        m_LastTimeStamp.clear();
        m_HasMoreContent = true;

        std::lock_guard<std::mutex> lock(m_ThreadMutex);
        if (m_Thread)
        {
            m_Thread->Cancel();
        }
    }

    bool RedditContentProvider::LoadingContent()
    {
        std::lock_guard<std::mutex> lock(m_ThreadMutex);
        return m_Thread != nullptr;
    }
}
