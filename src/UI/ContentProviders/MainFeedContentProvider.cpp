#include <AppSettings.h>
#include "MainFeedContentProvider.h"
#include "UI/Widgets/PostWidget.h"
#include "API/RedditAPI.h"
#include "Application.h"
#include "util/ThreadWorker.h"

namespace ui
{
    MainFeedContentProvider::MainFeedContentProvider()
    {
    }

    std::vector<RedditContentListItemRef> MainFeedContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<::api::PostRef> posts = ::api::RedditAPI::Get()->GetPosts(Application::Get()->GetPostFeedSorting(), AppSettings::Get()->GetInt("num_posts_to_load", 5), lastTimeStamp);
        g_info("MainFeedContentProvider::GetContentInternal - API returned %zu items", posts.size());
        std::vector<RedditContentListItemRef> uiElements;
        for (const api::PostRef& post : posts)
        {
            if (worker->IsCancelled())
            {
                g_info("MainFeedContentProvider::GetContentInternal - Worker cancelled, retuning %zu items", uiElements.size());
                return uiElements;
            }

            PostWidgetRef postUI = std::make_shared<PostWidget>(post);
            uiElements.push_back(postUI);
        }

        return uiElements;
    }

    std::string MainFeedContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No posts found. Retry?";
    }
}