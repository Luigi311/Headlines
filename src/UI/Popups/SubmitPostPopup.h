#include "Popup.h"
#include "UI/ui_types_fwd.h"
#include "API/api_fwd.h"

namespace util
{
    class ThreadWorker;
    class WebSocket;
}

namespace ui
{
    class SubmitPostPopup : public Popup
    {
    public:
        enum class WritePostType
        {
            Text,
            Link,
            Image,
            Video
        };

        explicit SubmitPostPopup(WritePostType type);

        void Reload() override {}
        void Cleanup() override {}
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override { return UISettings(); }

    private:
        void OnPopupChoiceSelected(GtkResponseType response) override;
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;

        enum class FileBrowserType
        {
            Image,
            Video
        };

        void CheckValid();

        void ShowSubredditPopover(Gtk::Widget* parent);
        void SetupSelectedSubreddit(const api::SubredditRef& subreddit);
        api::FlairRef GetSelectedFlair();

        void TrySubmitPost();
        void ShowFileBrowser(FileBrowserType type, const std::function<void(const std::shared_ptr<Gio::File>&)>& onFileSelected);
        void UploadMedia(const std::shared_ptr<Gio::File>& file, bool video, bool thumbnail);

        static void ShowErrorMessage(const std::string& errorMsg);

        WritePostType m_Type;

        std::mutex m_Mutex;
        struct UploadThread
        {
            std::thread* m_Thread = nullptr;
            util::ThreadWorker* m_Worker = nullptr;
        };

        Glib::Dispatcher m_MainThreadDispatcher;
        std::vector<UploadThread*> m_Threads;
        std::vector<UploadThread*> m_CompletedThreads;
        std::vector<std::pair<std::string, util::WebSocket*>> m_MediaResults;
        std::unordered_map<std::shared_ptr<Gio::File>, Gtk::Spinner*> m_PreviewSpinners;

        //TODO: this is ugly
        std::pair<std::string, util::WebSocket*> m_VideoMediaResult;
        std::pair<std::string, util::WebSocket*> m_VideoThumbnailMediaResult;

        Gtk::Box* m_SubredditBox = nullptr;
        Gtk::Button* m_AddImageButton = nullptr;
        Gtk::Button* m_AddVideoButton = nullptr;
        Gtk::ToggleButton* m_NSFWButton = nullptr;
        Gtk::ToggleButton* m_SpoilerButton = nullptr;
        Gtk::Entry* m_TitleEntry = nullptr;
        Gtk::TextView* m_TextView = nullptr;
        Gtk::Label* m_ContentLabel = nullptr;
        Gtk::Box* m_ImagesBox = nullptr;
        SearchResultWidgetRef m_SelectedSubreddit = nullptr;
        Gtk::Button* m_SelectSubredditButton = nullptr;
        Gtk::DropDown* m_FlairDropDown = nullptr;
        std::vector<api::FlairRef> m_SubredditFlairs;
        Gtk::Popover* m_SubredditPopover = nullptr;
        RedditContentListBoxViewRef m_PopoverContents = nullptr;
    };
}
