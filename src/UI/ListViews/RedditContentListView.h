#pragma once
#include "pch.h"
#include "UI/ui_types_fwd.h"
#include "util/ThreadWorker.h"

namespace util
{
    class SimpleThread;
    class ThreadWorker;
}

namespace ui
{
    class RedditContentListItem;
    class RedditContentListView
    {
    public:
        explicit RedditContentListView(const RedditContentProviderRef& contentProvider);
        virtual ~RedditContentListView();

        virtual void CreateUI(Gtk::Box* parent, Gtk::Viewport* parentViewport);
        void LoadContentAsync();
        void ClearContent();

    protected:
        Gtk::Box* m_ContentContainer;
        Gtk::Spinner* m_Spinner;
        std::vector<RedditContentListItemRef> m_ContentItems;

    private:
        virtual void AddContentToUI(std::vector<RedditContentListItemRef> newContent);
        void AddFailedToLoadUI();

        void OnViewportScrolled();
        void UpdateVisibleContent();

        //State
        RedditContentProviderRef m_ContentProvider;

        //UI
        Gtk::Viewport* m_Viewport;
        Gtk::Overlay* m_FailedToLoadWidget;

        //used to track enabling / disabling list items
        int m_LastViewportAdjustment;
        int m_FirstVisibleItemIndex;
        int m_LastVisibleItemIndex;
    };
}
