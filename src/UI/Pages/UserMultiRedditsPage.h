#pragma once

#include "Page.h"

namespace ui
{
    class UserMultiRedditsPage : public Page
    {
    public:
        explicit UserMultiRedditsPage(std::function<void(const SearchResultWidget*)> onClickOverride = nullptr);
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override;
    private:
        virtual void OnHeaderBarCreated() override;
        ui::UserMultiRedditsContentProviderRef m_ContentProvider;
        ui::RedditContentListBoxViewRef m_ListView;
        std::function<void(const SearchResultWidget*)> m_OnClickOverride;
    };
}
