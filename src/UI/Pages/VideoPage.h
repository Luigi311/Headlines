#pragma once

#include "UI/Pages/Page.h"

namespace ui
{
    class VideoPage : public Page
    {
    public:
        explicit VideoPage();
        void Setup(const std::string& url);

        virtual void Reload() override;
        virtual void Cleanup() override;
        virtual SortType GetSortType() const override;
        virtual UISettings GetUISettings() const override;
        virtual void OnDeactivate() override;

        void OnNotify(const std::string& event);
        void SetControlsVisible(bool visible, bool addHideTimeout);
    private:
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        void SetupForYoutube(const std::string& url);
        void SetupForDASH(const std::string& url);

        void OnVideoUrlReady();
        void SetupDuration(double seconds);

        xmlDocPtr GetDASHXml();
        static std::string GetTimeStringFromDASHXml(xmlDocPtr dashDoc);
        static void ParseDASHTimeString(std::string timeString, double& seconds);

        std::string m_Url;
        bool m_ControlsVisible = false;
        guint m_PendingTimeoutID = 0;
        bool m_MediaPrepared = false;

        std::thread* m_UrlPrepThread = nullptr;
        Glib::Dispatcher m_VideoUrlReadyDispatcher;

        Glib::RefPtr<Gtk::EventControllerMotion> m_MotionEventController = nullptr;
        Gtk::Label* m_CurrTimeLabel = nullptr;
        Gtk::Label* m_DurationLabel = nullptr;
        Gtk::Scale* m_SeekBar = nullptr;
        Gtk::Overlay* m_Overlay = nullptr;
        Gtk::Picture* m_Video = nullptr;
        Gtk::Image* m_PlayPauseImage = nullptr;
        Gtk::Spinner* m_Spinner = nullptr;
        Glib::RefPtr<Gtk::MediaFile> m_MediaFile = nullptr;
    };
}