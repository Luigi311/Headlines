#include "HiddenPostsPage.h"
#include "AppSettings.h"
#include "UI/ListViews/RedditContentListView.h"
#include "UI/ContentProviders/UserHiddenPostsContentProvider.h"

namespace ui
{
    HiddenPostsPage::HiddenPostsPage()
        : Page(PageType::HiddenPage)
        , m_ListView(std::make_shared<RedditContentListView>(std::make_shared<UserHiddenPostsContentProvider>()))
    {

    }

    void HiddenPostsPage::Cleanup()
    {
    }

    Gtk::Box*  HiddenPostsPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/posts_view.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("PostsView");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        Gtk::Viewport* postsViewport;
        postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox;
        postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_ListView->CreateUI(postsBox, postsViewport);
        m_ListView->LoadContentAsync();

        return box;
    }

    void HiddenPostsPage::Reload()
    {
        m_ListView->ClearContent();
        m_ListView->LoadContentAsync();
    }

    UISettings HiddenPostsPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            true,
            false
        };
    }
}