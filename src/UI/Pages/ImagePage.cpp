#include "ImagePage.h"

#include <utility>
#include "AppSettings.h"
#include "util/ImageDownloader.h"
#include "util/Helpers.h"

namespace ui
{

    ImagePage::ImagePage(const std::vector<util::ImageCollection>& images)
        : Page(PageType::ImagePage)
        , m_Images(images)
        , m_OriginalPictureWidth(-1)
        , m_OriginalPictureHeight(-1)
    {

    }

    Gtk::Box*  ImagePage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/image.ui");
        m_Notebook = builder->get_widget<Gtk::Notebook>("Notebook");
        for (const util::ImageCollection& imageCollection : m_Images)
        {
            if (imageCollection.m_Images.empty())
            {
                continue;
            }

            Gtk::Picture* picture = new Gtk::Picture();
            picture->set_hexpand(true);
            picture->set_vexpand(true);

            m_Notebook->append_page(*picture);

            Glib::RefPtr<Gtk::EventControllerScroll> scroll = Gtk::EventControllerScroll::create();
            picture->add_controller(scroll);
            scroll->set_flags(Gtk::EventControllerScroll::Flags::VERTICAL);
            scroll->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);

            scroll->signal_scroll().connect([this, picture](double, double y)
            {
                if (m_OriginalPictureWidth == -1)
                {
                    m_OriginalPictureWidth = picture->get_width();
                }

                if (m_OriginalPictureHeight == -1)
                {
                    m_OriginalPictureHeight = picture->get_height();
                }

                m_Scale += (float)-y * 0.1f;
                if (m_Scale < 1.f)
                {
                    m_Scale = 1.f;
                   return true;
                }

                picture->set_hexpand(false);
                picture->set_vexpand(false);
                picture->set_size_request((int)(m_OriginalPictureWidth * m_Scale), (int)(m_OriginalPictureHeight * m_Scale));
                picture->queue_draw();
                return true;
            }, true);

            Glib::RefPtr<Gtk::GestureZoom> zoom = Gtk::GestureZoom::create();
            picture->add_controller(zoom);
            zoom->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);
            zoom->signal_scale_changed().connect([this, picture](double scale)
            {
                if (m_OriginalPictureWidth == -1)
                {
                    m_OriginalPictureWidth = picture->get_width();
                }

                if (m_OriginalPictureHeight == -1)
                {
                    m_OriginalPictureHeight = picture->get_height();
                }

                picture->set_hexpand(false);
                picture->set_vexpand(false);
                picture->set_size_request((int)(m_OriginalPictureWidth * scale), (int)(m_OriginalPictureHeight * scale));
                picture->queue_draw();
            });

            const util::ImageData& imageData = imageCollection.GetImageData(util::ImageQuality::Highest);
            util::ImageDownloader* imageDownloader = new util::ImageDownloader();
            imageDownloader->DownloadImageAsync(imageData, [picture, imageDownloader](const std::string& imagePath)
            {
                picture->set_filename(imagePath);
                delete imageDownloader;
            });

            m_Pictures.push_back(picture);
        }

        Gtk::Button* leftButton = builder->get_widget<Gtk::Button>("ButtonLeft");
        leftButton->set_visible(m_Images.size() > 1);
        leftButton->signal_clicked().connect([this]()
        {
            int imageIndex = m_Notebook->get_current_page();
            imageIndex--;
            if (imageIndex < 0)
            {
                imageIndex = m_Notebook->get_n_pages() - 1;
            }

            m_Notebook->set_current_page(imageIndex);
        });
        Gtk::Button* rightButton = builder->get_widget<Gtk::Button>("ButtonRight");
        rightButton->set_visible(m_Images.size() > 1);
        rightButton->signal_clicked().connect([this]()
        {
            int imageIndex = m_Notebook->get_current_page();
            imageIndex++;
            if (imageIndex >= m_Notebook->get_n_pages())
            {
                imageIndex = 0;
            }

            m_Notebook->set_current_page((int)imageIndex);
        });

        Gtk::Box* box = builder->get_widget<Gtk::Box>("Box");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        return box;
    }

    UISettings ImagePage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            false,
            false,
            false,
            true
        };
    }

    void ImagePage::Cleanup()
    {
    }

    void ImagePage::OnSaveButton()
    {
        Gtk::FileChooserDialog* fileChooser = new Gtk::FileChooserDialog("Save Image...", Gtk::FileChooserDialog::Action::SAVE, true);
        fileChooser->add_button("Save", Gtk::ResponseType::ACCEPT);
        fileChooser->add_button("Cancel", Gtk::ResponseType::CANCEL);
        fileChooser->set_current_name(util::Helpers::GetFileName(m_Pictures[m_Notebook->get_current_page()]->get_file()->get_path()));
        Glib::RefPtr<Gio::File> homeFolder = Gio::File::create_for_path(std::getenv("HOME"));
        fileChooser->set_current_folder(homeFolder);
        fileChooser->show();
        fileChooser->signal_response().connect([this, fileChooser](int response)
        {
            if ((Gtk::ResponseType)response == Gtk::ResponseType::ACCEPT)
            {
                std::filesystem::copy(m_Pictures[m_Notebook->get_current_page()]->get_file()->get_path(), fileChooser->get_file()->get_path(), std::filesystem::copy_options::overwrite_existing);
            }

            fileChooser->close();
        });
    }
}
