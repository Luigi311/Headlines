#include <API/RedditAPI.h>
#include <util/Signals.h>
#include <Application.h>
#include "SubredditPage.h"
#include "API/Subreddit.h"
#include "util/Helpers.h"
#include "util/HtmlParser.h"
#include "AppSettings.h"
#include "util/SimpleThread.h"
#include "UI/Widgets/SubredditWidget.h"
#include "UI/Widgets/HeaderBar.h"
#include "UI/ContentProviders/SubredditPostsContentProvider.h"
#include "UI/Pages/UserMultiRedditsPage.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "API/MultiReddit.h"

namespace ui
{
    SubredditPage::SubredditPage(const api::SubredditRef& subreddit)
            : Page(PageType::SubredditPage)
            , m_Subreddit(subreddit)
            , m_ContentProvider(std::make_shared<SubredditPostsContentProvider>())
            , m_PostsView(std::make_shared<RedditContentListView>(m_ContentProvider))
    {
        SetupActions();
    }

    SubredditPage::SubredditPage(const std::string& subredditName)
            : Page(PageType::SubredditPage)
            , m_Subreddit(nullptr)
            , m_PostsView()
    {
        struct ThreadStorage
        {
            api::SubredditRef subreddit;
        };
        ThreadStorage* storage = new ThreadStorage();
        m_Threads.push_back(new util::SimpleThread([storage, subredditName](util::ThreadWorker* worker)
        {
            if (!worker->IsCancelled())
            {
                storage->subreddit = api::RedditAPI::Get()->GetSubreddit(subredditName);
            }
        },
        [this, storage](bool cancelled, util::SimpleThread* thread)
        {
            if (!cancelled)
            {
                m_Threads.erase(std::remove(m_Threads.begin(), m_Threads.end(), thread), m_Threads.end());
                SetSubreddit(storage->subreddit);
            }
            delete storage;
        }));

        SetupActions();
    }

    void SubredditPage::Cleanup()
    {
        for (util::SimpleThread* thread : m_Threads)
        {
            thread->Cancel();
        }

        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }
    }

    void SubredditPage::Reload()
    {
        m_PostsView->ClearContent();
        m_PostsView->LoadContentAsync();
    }

    Gtk::Box*  SubredditPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/subreddit_page.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("SubredditViewBox");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        m_Spinner = builder->get_widget<Gtk::Spinner>("Spinner");
        if (m_Subreddit)
        {
            m_Spinner->set_visible(false);
        }

        Gtk::Box* sidebarBox = builder->get_widget<Gtk::Box>("SidebarBox");
        m_SidebarSubredditWidget = std::make_shared<ui::SubredditWidget>();
        m_SidebarSubredditWidget->CreateUI(sidebarBox, true);
        m_Sidebar = builder->get_widget<Gtk::Box>("Sidebar");

        bool rounded = AppSettings::Get()->GetBool("rounded_corners", false);
        util::Helpers::ApplyCardStyle(m_Sidebar, rounded);
        m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool roundedCorners)
        {
            util::Helpers::ApplyCardStyle(m_Sidebar, roundedCorners);
        });

        m_Stack = (AdwViewStack*)builder->get_widget<Gtk::Widget>("Notebook")->gobj();
        m_SwitcherBar = (AdwViewSwitcherBar *)builder->get_widget<Gtk::Widget>("switcher_bar")->gobj();
        Gtk::Viewport* postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_PostsSubredditWidget = std::make_shared<ui::SubredditWidget>();
        m_PostsSubredditWidget->CreateUI(postsBox, false);
        m_PostsView->CreateUI(postsBox, postsViewport);

        if (m_Subreddit)
        {
            SetSubreddit(m_Subreddit);
        }

        return box;
    }

    void SubredditPage::SetSubreddit(const api::SubredditRef& subreddit)
    {
        if (m_Spinner)
        {
            ((Gtk::Box*)m_Spinner->get_parent())->remove(*m_Spinner);
            m_Spinner = nullptr;
        }

        m_Subreddit = subreddit;
        m_ContentProvider->SetSubreddit(subreddit);

        if (m_SidebarSubredditWidget)
        {
            m_SidebarSubredditWidget->SetSubreddit(subreddit);
            m_SidebarSubredditWidget->SetVisible(true);
        }

        if (m_PostsSubredditWidget)
        {
            m_PostsSubredditWidget->SetSubreddit(subreddit);
            m_PostsSubredditWidget->SetVisible(true);
        }

        util::HtmlParser htmlParser;
        htmlParser.ParseHtml(m_Subreddit->GetDescription(), m_Sidebar, -1, true);

        m_PostsView->ClearContent();
        m_PostsView->LoadContentAsync();
    }

    UISettings SubredditPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            true,
            false,
            true,
            false,
            true,
            false
        };
    }

    void SubredditPage::OnHeaderBarCreated()
    {
        AdwViewSwitcherTitle* viewSwitcher = GetHeaderBar()->AddViewSwitcher(m_Stack);
        g_object_bind_property(viewSwitcher, "title-visible", m_SwitcherBar, "reveal", GBindingFlags::G_BINDING_SYNC_CREATE);
    }

    void SubredditPage::SetupActions()
    {
        AddAction("Add to Multireddit", [this]()
        {
            ui::UserMultiRedditsPageRef userMultiRedditsPage = std::make_shared<ui::UserMultiRedditsPage>([this](const SearchResultWidget* widget)
                                                                                                          {
                                                                                                              const api::MultiRedditRef& multiReddit = widget->GetMultiReddit();
                                                                                                              if (multiReddit && std::find(multiReddit->GetSubreddits().begin(), multiReddit->GetSubreddits().end(), m_Subreddit->GetDisplayName()) == multiReddit->GetSubreddits().end())
                                                                                                              {
                                                                                                                  api::RedditAPI::Get()->AddSubredditToMultiReddit(m_Subreddit, multiReddit);
                                                                                                                  Application::Get()->NavigateBackwards();
                                                                                                              }
                                                                                                          });
            Application::Get()->AddPage(userMultiRedditsPage);
            userMultiRedditsPage->GetHeaderBar()->SetTitle("Select Multireddit");
        });
    }
}