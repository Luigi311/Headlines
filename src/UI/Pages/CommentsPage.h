#pragma once

#include "Page.h"
#include "API/api_fwd.h"

namespace util
{
    class SimpleThread;
}

namespace ui
{
    class CommentsPage : public Page
    {
    public:
        CommentsPage();
        explicit CommentsPage(const std::string& postUrl);
        ~CommentsPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override;
        virtual UISettings GetUISettings() const override;

        void SetPost(const api::PostRef& post);

    private:
        Gtk::Box* m_Box = nullptr;
        Gtk::Viewport* m_Viewport = nullptr;
        ui::CommentsViewRef m_CommentView;

        std::vector<util::SimpleThread*> m_Threads;
    };
}
