#include "SearchPage.h"
#include "AppSettings.h"
#include "Application.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "SubredditPage.h"
#include "UserPage.h"
#include "UI/Widgets/HeaderBar.h"
#include "UI/ContentProviders/SearchPostsContentProvider.h"
#include "UI/ContentProviders/UserSearchContentProvider.h"
#include "UI/ContentProviders/SubredditSearchContentProvider.h"
#include "UI/ListViews/RedditContentListBoxView.h"

namespace ui
{
    SearchPage::SearchPage()
            : Page(PageType::SearchPage)
            , m_UserSearchProvider(std::make_shared<UserSearchContentProvider>(this))
            , m_SubredditSearchProvider(std::make_shared<SubredditSearchContentProvider>(this))
            , m_PostsView(std::make_shared<RedditContentListView>(std::make_shared<SearchPostsContentProvider>(this)))
            , m_SubredditView(std::make_shared<RedditContentListBoxView>(m_SubredditSearchProvider))
            , m_UsersView(std::make_shared<RedditContentListBoxView>(m_UserSearchProvider))
    {

    }

    void SearchPage::Cleanup()
    {
    }

    void SearchPage::Reload()
    {
        m_PostsView->ClearContent();
        m_SubredditView->ClearContent();
        m_UsersView->ClearContent();

        m_PostsView->LoadContentAsync();
        m_SubredditView->LoadContentAsync();
        m_UsersView->LoadContentAsync();
    }

    Gtk::Box* SearchPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/search_view.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("SearchViewBox");

        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        m_Stack = (AdwViewStack*)builder->get_widget<Gtk::Widget>("SearchViewStack")->gobj();
        m_ViewSwitcherBar = (AdwViewSwitcherBar *)builder->get_widget<Gtk::Widget>("switcher_bar")->gobj();

        Gtk::Viewport* postsViewport;
        postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_PostsView->CreateUI(postsBox, postsViewport);

        Gtk::Viewport* subsViewport;
        subsViewport = builder->get_widget<Gtk::Viewport>("SubredditsViewport");
        Gtk::Box* subsBox = builder->get_widget<Gtk::Box>("SubredditBox");
        m_SubredditView->CreateUI(subsBox, subsViewport);
        m_SubredditSearchProvider->SetItemClickHandler([](const SearchResultWidget* searchResult)
        {
            ui::SubredditPageRef subView = std::make_shared<ui::SubredditPage>(searchResult->GetSubreddit());
            Application::Get()->AddPage(subView);
        });

        Gtk::Viewport* usersViewport;
        usersViewport = builder->get_widget<Gtk::Viewport>("UsersViewport");
        Gtk::Box* usersBox = builder->get_widget<Gtk::Box>("UsersBox");
        m_UsersView->CreateUI(usersBox, usersViewport);
        m_UserSearchProvider->SetItemClickHandler([](const SearchResultWidget* searchResult)
        {
            ui::UserPageRef userView = std::make_shared<ui::UserPage>(searchResult->GetUser());
            Application::Get()->AddPage(userView);
        });

        return box;
    }

    void SearchPage::SetSearchString(const std::string& string)
    {
        m_SearchString = string;
        std::replace(m_SearchString.begin(), m_SearchString.end(), ' ', '+');
        if (!string.empty())
        {
            m_PostsView->ClearContent();
            m_PostsView->LoadContentAsync();

            m_SubredditView->ClearContent();
            m_SubredditView->LoadContentAsync();

            m_UsersView->ClearContent();
            m_UsersView->LoadContentAsync();
        }
    }

    UISettings SearchPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            true,
            false
        };
    }

    void SearchPage::OnHeaderBarCreated()
    {
        AdwViewSwitcherTitle* viewSwitcher = GetHeaderBar()->AddViewSwitcher(m_Stack);
        g_object_bind_property(viewSwitcher, "title-visible", m_ViewSwitcherBar, "reveal", GBindingFlags::G_BINDING_SYNC_CREATE);

        GetHeaderBar()->SetTitle("Search");
    }
}