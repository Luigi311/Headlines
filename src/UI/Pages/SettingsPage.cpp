#include "SettingsPage.h"
#include "AppSettings.h"
#include "util/Signals.h"

namespace ui
{
    SettingsPage::SettingsPage()
     : Page(PageType::SettingsPage)
     , m_RoundedCornersSettingChangedHandler(util::s_InvalidSignalHandlerID)
    {

    }

    void SettingsPage::Cleanup()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }
    }


    void SettingsPage::EnsureCategoryExists(const std::string& category)
    {
        if (std::find(m_Categories.begin(), m_Categories.end(), category) == m_Categories.end())
        {
            m_Categories.push_back(category);
        }
    }

    void SettingsPage::AddWidgetCommon(const std::string& name, const std::string& description, const std::string& category, Gtk::Widget* widget)
    {
        if (m_CategoryBoxes.find(category) == m_CategoryBoxes.end())
        {
            Gtk::ListBox* listBox = new Gtk::ListBox();
            listBox->set_selection_mode(Gtk::SelectionMode::NONE);
            listBox->get_style_context()->add_class("content");
            m_CategoryBoxes[category] = listBox;
        }

        GtkWidget* actionRow = adw_action_row_new();
        adw_action_row_add_suffix((AdwActionRow*)actionRow, widget->gobj());
        adw_preferences_row_set_title((AdwPreferencesRow*)actionRow, name.c_str());
        if (!description.empty())
        {
            adw_action_row_set_subtitle((AdwActionRow*)actionRow, description.c_str());
        }

        auto it = std::find_if(m_SettingsWidgets.begin(), m_SettingsWidgets.end(),
        [category](const SettingsCategory& settingsCategory)
        {
            return settingsCategory.m_Name == category;
        });

        if (it != m_SettingsWidgets.end())
        {
            (*it).m_SettingsWidgets.push_back(actionRow);
        }
        else
        {
            m_SettingsWidgets.push_back(SettingsCategory { category, std::vector<GtkWidget*>() });
            m_SettingsWidgets.back().m_SettingsWidgets.push_back(actionRow);
        }
    }

    void SettingsPage::AddToggleSetting(const std::string& name, const std::string& description, const std::string& category, const std::string& setting_name, bool defaultVal, const std::function<void()>& onChanged)
    {
        EnsureCategoryExists(category);
        Gtk::Switch* gtk_switch = new Gtk::Switch();
        gtk_switch->set_valign(Gtk::Align::CENTER);
        gtk_switch->set_state(AppSettings::Get()->GetBool(setting_name, defaultVal));
        gtk_switch->signal_state_set().connect([setting_name, onChanged](bool set)
        {
            AppSettings::Get()->SetBool(setting_name, set);
            if (onChanged)
            {
                onChanged();
            }
            return false;
        }, false);

        AddWidgetCommon(name, description, category, gtk_switch);
    }

    void SettingsPage::AddIntSetting(const std::string& name, const std::string& description, const std::string& category, const std::string& setting_name, int defaultVal, const std::function<void()>& onChanged)
    {
        EnsureCategoryExists(category);

        Glib::RefPtr<Gtk::Adjustment> adjustment = Gtk::Adjustment::create(AppSettings::Get()->GetInt(setting_name, defaultVal), 0, 100);
        Gtk::SpinButton* spinButton = new Gtk::SpinButton();
        spinButton->set_valign(Gtk::Align::CENTER);
        spinButton->set_adjustment(adjustment);
        spinButton->signal_value_changed().connect([spinButton, setting_name, onChanged]()
        {
            AppSettings::Get()->SetInt(setting_name, spinButton->get_value_as_int());
            if (onChanged)
            {
                onChanged();
            }
            }, false);

        AddWidgetCommon(name, description, category, spinButton);
    }

    Gtk::Box* SettingsPage::CreateUIInternal(AdwLeaflet* parent)
    {
        m_Box = new Gtk::Box();
        m_Box->set_vexpand(true);
        m_Box->set_orientation(Gtk::Orientation::VERTICAL);

        Gtk::ScrolledWindow* window = new Gtk::ScrolledWindow();
        window->set_vexpand(true);
        m_Box->append(*window);

        Gtk::Viewport* viewport = new Gtk::Viewport(Gtk::Adjustment::create(0, 0, 0), Gtk::Adjustment::create(0, 0, 0));
        viewport->set_vexpand(true);
        window->set_child(*viewport);

        AdwClamp* clamp = (AdwClamp*)adw_clamp_new();
        adw_clamp_set_maximum_size(clamp, 900);
        adw_clamp_set_tightening_threshold(clamp, 900);
        gtk_viewport_set_child(viewport->gobj(), (GtkWidget*)clamp);

        m_ListsBox = new Gtk::Box();
        m_Box->set_valign(Gtk::Align::FILL);
        m_ListsBox->set_margin_start(5);
        m_ListsBox->set_margin_end(5);
        m_ListsBox->set_halign(Gtk::Align::FILL);
        m_ListsBox->set_orientation(Gtk::Orientation::VERTICAL);
        m_ListsBox->set_spacing(10);

        if (!AppSettings::Get()->GetBool("rounded_corners", false))
        {
            m_ListsBox->get_style_context()->add_class("no_rounded_corners");
        }

        m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool rounded)
        {
            if (rounded && m_ListsBox->get_style_context()->has_class("no_rounded_corners"))
            {
                m_ListsBox->get_style_context()->remove_class("no_rounded_corners");
            }
            else if (!rounded && !m_ListsBox->get_style_context()->has_class("no_rounded_corners"))
            {
                m_ListsBox->get_style_context()->add_class("no_rounded_corners");
            }
        });

        adw_clamp_set_child(clamp, (GtkWidget*)m_ListsBox->gobj());

        // General
        AddToggleSetting("Dark mode", "", "General", "use_dark_theme", false, []()
        {
            if (AppSettings::Get()->GetBool("use_dark_theme", false))
            {
                adw_style_manager_set_color_scheme(adw_style_manager_get_default(), ADW_COLOR_SCHEME_PREFER_DARK);
            }
            else
            {
                adw_style_manager_set_color_scheme(adw_style_manager_get_default(), ADW_COLOR_SCHEME_DEFAULT);
            }
        });
        AddToggleSetting("Rounded corners", "May affect performance.", "General", "rounded_corners", false, []()
        {
            util::Signals::Get()->RoundedCornersSettingChanged.Fire(AppSettings::Get()->GetBool("rounded_corners", false));
        });
#if !WIN32
        AddToggleSetting("Use libsecret to store password", "Disabling this is not secure but should fix any issues you are having staying logged in.", "General", "use_libsecret", true);
#endif

        // Posts
        AddIntSetting("Number of posts to load at once", "", "Posts", "num_posts_to_load", 5);
	    AddToggleSetting("Blur NSFW Images", "May affect performance.","Posts", "hide_nsfw", true);
	    AddToggleSetting("Blur spoilers", "May affect performance.","Posts", "hide_spoiler", true);

	    // Comments
	    AddIntSetting("Number of comments to load at once", "", "Comments", "num_comments_to_load", 20);
	    AddToggleSetting("Fully collapse comments", "","Comments", "full_comment_collapse", false);

        // Debug
        AddToggleSetting("Log API responses", "", "Debug", "print_api_responses", false);

        SetupPage();

        m_LeafletPage= adw_leaflet_append(parent, (GtkWidget*)m_Box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        return m_Box;
    }

    void SettingsPage::Reload()
    {
        // do nothing
    }

    SortType SettingsPage::GetSortType() const
    {
        return SortType::None;
    }

    UISettings SettingsPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            false,
            false
        };
    }

    void SettingsPage::SetupPage()
    {
        for (const auto& [category, widgets] : m_SettingsWidgets)
        {
            CreateTitleWidget(category);
            m_ListsBox->append(*m_CategoryBoxes[category]);
            for (const auto& widget : widgets)
            {
                gtk_list_box_append(m_CategoryBoxes[category]->gobj(), widget);
            }
        }
    }

    void SettingsPage::CreateTitleWidget(const std::string& category)
    {
        Gtk::Label* label = new Gtk::Label();
        label->set_justify(Gtk::Justification::LEFT);
        label->set_halign(Gtk::Align::START);
        label->set_text(category);
        label->set_margin_top(10);
        Pango::AttrList attrList;
        Pango::AttrInt fontWeight = Pango::AttrInt::create_attr_weight(Pango::Weight::BOLD);
        attrList.insert(fontWeight);
        label->set_attributes(attrList);
        m_ListsBox->append(*label);
    }
}
