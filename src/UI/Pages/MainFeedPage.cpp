#include "MainFeedPage.h"
#include "AppSettings.h"
#include "util/Signals.h"
#include "UI/ListViews/RedditContentListView.h"
#include "UI/ContentProviders/MainFeedContentProvider.h"

namespace ui
{
    MainFeedPage::MainFeedPage()
    : Page(PageType::MainFeedPage)
    , m_ListView(std::make_shared<RedditContentListView>(std::make_shared<MainFeedContentProvider>()))
    , m_LoginStatusChangedHandler(util::s_InvalidSignalHandlerID)
    {
    }

    MainFeedPage::~MainFeedPage()
    {
        if (m_LoginStatusChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->LoginStatusChanged.RemoveHandler(m_LoginStatusChangedHandler);
        }
    }

    void MainFeedPage::Cleanup()
    {
    }

    void MainFeedPage::Reload()
    {
        m_ListView->ClearContent();
        m_ListView->LoadContentAsync();
    }

    Gtk::Box*  MainFeedPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/posts_view.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("PostsView");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        Gtk::Viewport* postsViewport;
        postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox;
        postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_ListView->CreateUI(postsBox, postsViewport);
        m_ListView->LoadContentAsync();

        m_LoginStatusChangedHandler = util::Signals::Get()->LoginStatusChanged.AddHandler([this](bool)
        {
            Reload();
        });

        return box;
    }

    UISettings MainFeedPage::GetUISettings() const
    {
        return UISettings
        {
            false,
            true,
            false,
            true,
            true,
            true,
            false
        };
    }
}