#include "PictureWidget.h"
#include "AppSettings.h"
#include "Application.h"
#include "util/SimpleThread.h"
#include "util/ThreadWorker.h"
#include "util/Helpers.h"
#include "util/ImageDownloader.h"

namespace ui
{
    PictureWidget::PictureWidget(int imageWidth, int imageHeight, bool crop, bool blur)
            : m_Crop(crop)
            , m_Blur(blur)
    {
        m_AspectRatio = (float) imageWidth / imageHeight;

        if (m_Crop)
        {
            m_AspectRatio = std::max(m_AspectRatio, 2.f);
        }
    }

    void PictureWidget::SetFilePath(const std::string& filepath)
    {
        m_FilePath = filepath;
        try
        {
            m_Pixbuf = Gdk::Pixbuf::create_from_file(m_FilePath);

            //sometimes reddit lies about the size of images.
            if (!m_Crop || m_AspectRatio > 2)
            {
                float realAspect = (float) m_Pixbuf->get_width() / m_Pixbuf->get_height();
                if (std::abs(realAspect - m_AspectRatio) > FLT_EPSILON)
                {
                    m_AspectRatio = realAspect;
                    queue_resize();
                }
            }

            if (m_Crop)
            {
                CropImage();
            }
            m_Texture = Gdk::Texture::create_for_pixbuf(m_Pixbuf);
        }
        catch (const Glib::Error& error)
        {
            std::filesystem::remove(filepath);
            g_info("%s %s", error.what(), filepath.c_str());

            if (++m_DownloadRetryCount < 10)
            {
                SetImageData(m_ImageData);
            }
        }
        queue_draw();
    }

    void PictureWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        if (m_Child)
        {
            Gdk::Rectangle rect = Gdk::Rectangle();
            rect.set_x(0);
            rect.set_y(0);
            rect.set_width(width);
            rect.set_height(height);

            m_Child->size_allocate(rect, baseline);
        }
    }

    void PictureWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int&, int&) const
    {
        if (orientation == Gtk::Orientation::VERTICAL)
        {
            minimum = natural = for_size / m_AspectRatio;
        }

        if (m_Child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_Child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void PictureWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_Blur)
        {
            snapshot->push_clip(Gdk::Rectangle(0, 0, get_width(), get_height()));
            snapshot->push_blur(20.f);
        }

        if (m_Texture)
        {
            m_Texture->snapshot(snapshot, get_allocated_width(), get_allocated_height());
        }

        if (m_Blur)
        {
            snapshot->pop();
            snapshot->pop();
        }

        if (m_Child)
        {
            snapshot_child(*m_Child, snapshot);
        }
    }

    Gtk::SizeRequestMode PictureWidget::get_request_mode_vfunc() const
    {
        return Gtk::SizeRequestMode::WIDTH_FOR_HEIGHT;
    }

    void PictureWidget::SetFromResource(const std::string& resourcePath)
    {
        m_ResourcePath = resourcePath;
        m_Pixbuf = Gdk::Pixbuf::create_from_resource(resourcePath);

        //sometimes reddit lies about the size of images.
        if (!m_Crop)
        {
            float realAspect = (float) m_Pixbuf->get_width() / m_Pixbuf->get_height();
            if (std::abs(realAspect - m_AspectRatio) > FLT_EPSILON)
            {
                m_AspectRatio = realAspect;
                queue_resize();
            }
        }

        if (m_Crop)
        {
            CropImage();
        }
        m_Texture = Gdk::Texture::create_for_pixbuf(m_Pixbuf);
        queue_draw();
    }

    void PictureWidget::SetImageData(const util::ImageData& imageData)
    {
        m_ImageData = imageData;

        if (m_ImageData.m_Url.find("/io/gitlab/caveman250/headlines") == 0)
        {
            SetFromResource(m_ImageData.m_Url);
            return;
        }

        if (m_ImageData.m_Url.empty())
        {
            SetFromResource("/io/gitlab/caveman250/headlines/default_subreddit.png");
            return;
        }

        if (!m_ImageDownloader)
        {
            m_ImageDownloader = new util::ImageDownloader();
        }
        m_ImageDownloader->DownloadImageAsync(m_ImageData, [this](const std::string& filePath)
        {
            SetFilePath(filePath);
        });
    }

    void PictureWidget::SetClickHandler(std::function<void()> clickhandler)
    {
        get_style_context()->add_class("image");
        m_OnClick = clickhandler;

        Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
        add_controller(gestureClick);
        gestureClick->signal_released().connect([this](int n_press, double, double)
                                                {
                                                    if (n_press == 0)
                                                        return;

                                                    m_OnClick();
                                                });
    }

    void PictureWidget::CropImage()
    {
        int heightDiff = m_Pixbuf->get_height() - m_Pixbuf->get_width() / m_AspectRatio;

        if (heightDiff > 0)
        {
            m_Pixbuf = Gdk::Pixbuf::create_subpixbuf(m_Pixbuf, 0, heightDiff / 2, m_Pixbuf->get_width(), m_Pixbuf->get_width() / m_AspectRatio);
        }
    }

    void PictureWidget::SetActive(bool active)
    {
        if (!active)
        {
            if (m_ImageDownloader)
            {
                m_ImageDownloader->Cancel();
            }

            m_Pixbuf.reset();
            m_Texture.reset();
        }
        else if (!m_Pixbuf)
        {
            if (!m_ResourcePath.empty())
            {
                SetFromResource(m_ResourcePath);
            }
            else
            {
                SetImageData(m_ImageData);
            }
        }
    }
}
