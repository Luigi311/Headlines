#pragma once

#include "API/Post.h"

namespace util
{
    class SimpleThread;
    class ThreadWorker;
    class ImageDownloader;
}

namespace ui
{
    class PictureWidget : public Gtk::Widget
    {
    public:
        PictureWidget(int imageWidth, int imageHeight, bool crop, bool blur);

        void SetImageData(const util::ImageData& imageData);
        void SetFromResource(const std::string& resourceName);
        void SetFilePath(const std::string& filePath);

        void SetClickHandler(std::function<void()> clickhandler);

        void SetActive(bool active);

    protected:
        virtual void size_allocate_vfunc(int width, int height, int baseline) override;

        virtual void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;

        virtual void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        virtual Gtk::SizeRequestMode get_request_mode_vfunc() const override;

        Gtk::Widget* m_Child = nullptr;

    private:

        void CropImage();

        Glib::RefPtr<Gdk::Pixbuf> m_Pixbuf = nullptr;
        Glib::RefPtr<Gdk::Texture> m_Texture = nullptr;
        float m_AspectRatio = 1;
        bool m_Crop = false;
        util::ImageData m_ImageData;
        std::string m_FilePath;
        std::string m_ResourcePath;
        std::function<void()> m_OnClick = nullptr;
        bool m_Blur = false;

        util::ImageDownloader* m_ImageDownloader = nullptr;
        int m_DownloadRetryCount = 0;
    };
}
