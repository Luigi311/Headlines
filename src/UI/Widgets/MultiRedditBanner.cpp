#include <AppSettings.h>
#include <util/Signals.h>
#include <util/HtmlParser.h>
#include "MultiRedditBanner.h"
#include "PictureWidget.h"
#include "API/MultiReddit.h"

namespace ui
{
    Gtk::Box* MultiRedditBanner::CreateMultiRedditBanner(Gtk::Box* parent, api::MultiRedditRef& multi, int& roundedCornersSignalHandler)
    {
        auto bannerBuilder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/multireddit.ui");

        Gtk::Box* banner = bannerBuilder->get_widget<Gtk::Box>("MultiredditPanel");
        bool rounded = AppSettings::Get()->GetBool("rounded_corners", false);
        util::Helpers::ApplyCardStyle(banner, rounded);
        roundedCornersSignalHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([banner](bool roundedCorners)
                                                                                                              {
                                                                                                                  util::Helpers::ApplyCardStyle(banner, roundedCorners);
                                                                                                              });
        parent->append(*banner);

        Gtk::Box* iconBox = bannerBuilder->get_widget<Gtk::Box>("MultiIconBox");
        util::ImageData imageData;
        imageData.m_Url = multi->GetIconUrl();
        imageData.m_Width = 96;
        imageData.m_Height = 96;
        PictureWidget* pictureWidget = new PictureWidget(imageData.m_Width, imageData.m_Height, false, false);
        pictureWidget->SetImageData(imageData);
        iconBox->append(*pictureWidget);

        Gtk::Label* nameLabel = bannerBuilder->get_widget<Gtk::Label>("NameLabel");
        nameLabel->set_text(multi->GetDisplayName());

        Gtk::Box* descBox = bannerBuilder->get_widget<Gtk::Box>("DescBox");
        util::HtmlParser htmlParser;
        htmlParser.ParseHtml(multi->GetDescriptionHtml(), descBox, -1, false);

        return banner;
    }
}