#include <AppSettings.h>
#include <util/Helpers.h>
#include "pch.h"
#include "HeaderBar.h"
#include "Application.h"
#include "UI/Pages/SearchPage.h"
#include "util/Signals.h"
#include "UI/Popups/SubmitPostPopup.h"

namespace ui
{
    HeaderBar::HeaderBar(bool forPopup)
        : m_Page(nullptr)
        , m_HeaderBar(nullptr)
        , m_BackButton(nullptr)
        , m_MenuButton(nullptr)
        , m_PostButton(nullptr)
        , m_SearchButton(nullptr)
        , m_SendButton(nullptr)
        , m_SortButton(nullptr)
        , m_TitleLabel(nullptr)
        , m_RefreshButton(nullptr)
        , m_ViewSwitcherTitle(nullptr)
        , m_DownloadButton(nullptr)
        , m_PageMenuButton(nullptr)
        , m_SearchBar(nullptr)
        , m_SearchEntry(nullptr)
        , m_SubmitMenu(nullptr)
        , m_SortMenus()
        , m_SortAction(nullptr)
        , m_SendButtonConnection()
        , m_FlapRevealConnection(0)
        , m_FlapFoldConnection(0)
        , m_LoginStatusChangedHandler(util::s_InvalidSignalHandlerID)
        , m_ForPopup(forPopup)
    {

    }

    HeaderBar::~HeaderBar()
    {
        if (m_LoginStatusChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->LoginStatusChanged.RemoveHandler(m_LoginStatusChangedHandler);
        }

        g_signal_handler_disconnect(Application::Get()->GetFlap(), m_FlapFoldConnection);
        g_signal_handler_disconnect(Application::Get()->GetFlap(), m_FlapRevealConnection);
    }

    static void FlapFoldedNotify(AdwFlap*, GParamSpec* , gpointer userData)
    {
        HeaderBar* headerBar = (HeaderBar*)userData;
        headerBar->OnWindowFoldStateChanged();
    }

    static void FlapRevealNotify(AdwFlap* flap, GParamSpec* , gpointer userData)
    {
        HeaderBar* headerBar = (HeaderBar*)userData;
        headerBar->OnWindowFoldStateChanged();
        AppSettings::Get()->SetBool("flap_revealed", adw_flap_get_reveal_flap(flap));
    }

    void HeaderBar::OnWindowFoldStateChanged()
    {
        AdwFlap* flap = Application::Get()->GetFlap();
        bool flapFolded = adw_flap_get_folded(flap);
        bool flapRevealed = adw_flap_get_reveal_flap(flap);

        bool visible = (m_Page->GetPageType() == PageType::MainFeedPage && !flapRevealed) ||
                       (!flapRevealed && !flapFolded);
        SetMenuButtonVisible(visible);

        adw_header_bar_set_show_start_title_buttons((AdwHeaderBar*)m_HeaderBar->gobj(), !m_ForPopup && (flapFolded || !flapRevealed));
    }

    void HeaderBar::CreateUI(const ui::Page* page, const std::vector<ui::SortType> sortTypes)
    {
        m_Page = page;

        const Glib::RefPtr<Gtk::Builder>& builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/headerbar.ui");
        m_HeaderBar = builder->get_widget<Gtk::Widget>("HeaderBar");

        OnWindowFoldStateChanged();

        SetupTitle(builder);
        SetupBackButton(builder);
        SetupMenuButton(builder);
        SetupPostButton(builder);
        SetupSortButton(builder, sortTypes);
        SetupRefreshButton(builder);
        SetupSearchButton(builder);
        SetupSendButton(builder);
        SetupDownloadButton(builder);
        SetupPageActions(builder);
        if (!sortTypes.empty())
        {
            SetupSortActions();
        }

        SetupSearchEntry();
        m_Page->GetContentBox()->prepend(*m_HeaderBar);

        if (m_SearchEntry && m_SearchEntry->get_visible())
        {
            m_SearchEntry->grab_focus();
        }
    }

    void HeaderBar::SetupBackButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        if (m_Page->GetUISettings().m_ShowBackButton)
        {
            m_BackButton = builder->get_widget<Gtk::Button>("BackButton");
            m_BackButton->signal_clicked().connect([]()
            {
                Application::Get()->NavigateBackwards();
            });
            m_BackButton->set_visible(true);
        }
    }

    void HeaderBar::SetupDownloadButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        if (m_Page->GetUISettings().m_ShowDownloadButton)
        {
            m_DownloadButton = builder->get_widget<Gtk::Button>("DownloadButton");
            m_DownloadButton->signal_clicked().connect([]()
                                                   {
                                                       Application::Get()->GetActivePage()->OnSaveButton();
                                                   });
            m_DownloadButton->set_visible(true);
        }
    }

    void HeaderBar::SetupPageActions(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        if (m_Page->GetMenuActions().size() > 0)
        {
            m_PageMenuButton = builder->get_widget<Gtk::MenuButton>("PageMenu");
            m_PageMenuButton->set_visible(true);

            auto menu = Gio::Menu::create();
            for (const auto&[actionName, actionId]: m_Page->GetMenuActions())
            {
                menu->append(actionName, actionId);
            }

            m_PageMenuButton->set_menu_model(menu);
        }
    }

    void HeaderBar::SetupMenuButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        bool revealFlap = adw_flap_get_reveal_flap(Application::Get()->GetFlap());
        bool flapFolded = adw_flap_get_folded(Application::Get()->GetFlap());

        m_MenuButton = builder->get_widget<Gtk::Button>("Menu");
        m_MenuButton->signal_clicked().connect([]()
        {
            adw_flap_set_reveal_flap(Application::Get()->GetFlap(), !adw_flap_get_reveal_flap(Application::Get()->GetFlap()));
        });
        m_FlapFoldConnection = g_signal_connect(Application::Get()->GetFlap(), "notify::folded", G_CALLBACK(FlapFoldedNotify), this);
        m_FlapRevealConnection = g_signal_connect(Application::Get()->GetFlap(), "notify::reveal-flap", G_CALLBACK(FlapRevealNotify), this);
        m_MenuButton->set_visible((m_Page->GetPageType() == PageType::MainFeedPage && !revealFlap) || (!revealFlap && !flapFolded));
    }

    void HeaderBar::SetupPostButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        if (m_Page->GetUISettings().m_ShowWritePostButton)
        {
            m_PostButton = builder->get_widget<Gtk::MenuButton>("Post");
            m_SubmitMenu = Gio::Menu::create();
            m_SubmitMenu->append("Text", "app.submit_text");
            m_SubmitMenu->append("Url", "app.submit_url");
            m_SubmitMenu->append("Image", "app.submit_image");
            m_SubmitMenu->append("Video", "app.submit_video");

            Application::Get()->GetGtkApplication()->add_action("submit_text", []()
            {
                ui::SubmitPostPopupRef submitPostPopup = std::make_shared<ui::SubmitPostPopup>(ui::SubmitPostPopup::WritePostType::Text);
                Application::Get()->AddPage(submitPostPopup);
            });
            Application::Get()->GetGtkApplication()->add_action("submit_url", []()
            {
                ui::SubmitPostPopupRef submitPostPopup = std::make_shared<ui::SubmitPostPopup>(ui::SubmitPostPopup::WritePostType::Link);
                Application::Get()->AddPage(submitPostPopup);
            });
            Application::Get()->GetGtkApplication()->add_action("submit_image", []()
            {
                ui::SubmitPostPopupRef submitPostPopup = std::make_shared<ui::SubmitPostPopup>(ui::SubmitPostPopup::WritePostType::Image);
                Application::Get()->AddPage(submitPostPopup);
            });
            Application::Get()->GetGtkApplication()->add_action("submit_video", []()
            {
                ui::SubmitPostPopupRef submitPostPopup = std::make_shared<ui::SubmitPostPopup>(ui::SubmitPostPopup::WritePostType::Video);
                Application::Get()->AddPage(submitPostPopup);
            });

            m_PostButton->set_menu_model(m_SubmitMenu);
            m_PostButton->set_sensitive(api::RedditAPI::Get()->IsLoggedIn());
            m_PostButton->set_visible(true);

            m_LoginStatusChangedHandler = util::Signals::Get()->LoginStatusChanged.AddHandler([this](bool loggedIn)
                    {
                if (m_PostButton)
                {
                    m_PostButton->set_sensitive(loggedIn);
                }
                    });
        }
    }

    void HeaderBar::SetupSortButton(const Glib::RefPtr<Gtk::Builder>& builder, const std::vector<ui::SortType> sortTypes)
    {
        if (m_Page->GetUISettings().m_ShowSortButton)
        {
            for (SortType sortType : sortTypes)
            {
                m_SortButton = builder->get_widget<Gtk::MenuButton>("FilterButton");
                Glib::RefPtr<Gio::Menu> sortMenu = Gio::Menu::create();
                m_SortMenus[sortType] = sortMenu;
                AddSortOptions(sortType);
            }

            m_SortButton->set_menu_model(m_SortMenus[m_Page->GetSortType()]);
            m_SortButton->set_visible(true);
        }
    }

    void HeaderBar::SetupRefreshButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        if (m_Page->GetUISettings().m_ShowRefreshButton)
        {
            m_RefreshButton = builder->get_widget<Gtk::Button>("RefreshButton");
            m_RefreshButton->signal_clicked().connect([]()
            {
                Application::Get()->GetActivePage()->Reload();
            });

            m_RefreshButton->set_visible(true);
        }
    }

    void HeaderBar::SetupSearchButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_SearchButton = builder->get_widget<Gtk::Button>("SearchButton");
        m_SearchButton->signal_clicked().connect([this]()
        {
            ShowSearchBar(!m_SearchBar->get_search_mode());
        });
        m_SearchButton->set_visible(!m_ForPopup);
    }

    void SearchActivate(GtkSearchEntry* entry, gpointer)
    {
        const char* entryText = gtk_editable_get_text((GtkEditable*)entry);
        if (entryText && strlen(entryText) > 0)
        {
            ui::PageRef page = Application::Get()->GetActivePage();
            if (page->GetPageType() == PageType::SearchPage)
            {
                std::static_pointer_cast<ui::SearchPage>(page)->SetSearchString(entryText);
            }
            else
            {
                ui::SearchPageRef searchPage = std::make_shared<ui::SearchPage>();
                Application::Get()->AddPage(searchPage);
                searchPage->SetSearchString(entryText);
            }

            page->GetHeaderBar()->ShowSearchBar(false);
        }
    }

    void HeaderBar::SetupSearchEntry()
    {
        m_SearchBar = new Gtk::SearchBar();
        m_SearchEntry = new Gtk::SearchEntry();
        m_SearchEntry->set_hexpand(true);
        g_signal_connect(m_SearchEntry->gobj(), "activate", G_CALLBACK(SearchActivate), nullptr);
        m_SearchBar->set_child(*m_SearchEntry);
        m_SearchBar->set_show_close_button(true);
        m_Page->GetContentBox()->prepend(*m_SearchBar);
    }

    void HeaderBar::SetupSendButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        if (m_Page->GetUISettings().m_ShowSendButton)
        {
            m_SendButton = builder->get_widget<Gtk::Button>("SendButton");
            m_SendButton->set_visible(true);
        }
    }

    void HeaderBar::SetupTitle(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_TitleLabel = (AdwWindowTitle*)builder->get_widget<Gtk::Widget>("TitleLabel")->gobj();

        if (!m_Page->GetUISettings().m_ShowTitle)
        {
            SetTitle("");
        }
    }

    void HeaderBar::SetSortState(int state)
    {
        m_SortAction->change_state(state);
    }

    void HeaderBar::SetupSortActions()
    {
        std::string sortPostsActionName = util::Helpers::FormatString("%s_sort", m_Page->GetID().c_str());
        if (VERIFY(!Application::Get()->GetGtkApplication()->has_action(sortPostsActionName), "Multiple pages trying to register the same sort actions."))
        {
            m_SortAction = Application::Get()->GetGtkApplication()->add_action_radio_integer(sortPostsActionName, [](int state)
            {
                Application::Get()->GetActivePage()->GetHeaderBar()->SetSortState(state);

                switch (Application::Get()->GetActivePage()->GetSortType())
                {
                    case ui::SortType::Posts:
                    case ui::SortType::SubredditPosts:
                    case ui::SortType::UserPosts:
                    case ui::SortType::MultiPosts:
                    {
                        Application::Get()->SetCurrentPostsSortType((::api::PostsType) state);
                        Application::Get()->GetActivePage()->GetHeaderBar()->SetTitle(api::PostsTypeToString((::api::PostsType)state));
                        break;
                    }
                    case ui::SortType::Comments:
                    case ui::SortType::UserComments:
                    {
                        Application::Get()->SetCurrentCommentsSortType((::api::CommentsType) state);
                        Application::Get()->GetActivePage()->GetHeaderBar()->SetTitle(api::CommentsTypeToString((::api::CommentsType)state));
                        break;
                    }
                    case ui::SortType::None:
                    {
                        //do nothing
                        break;
                    }
                }
            }, 0);
        }

        switch (m_Page->GetPageType())
        {
            case ui::PageType::MainFeedPage:
                SetCurrentPostsSortType(Application::Get()->GetPostFeedSorting());
                break;
            case ui::PageType::CommentsPage:
                SetCurrentCommentsSortType(Application::Get()->GetCommentsViewSorting());
                break;
            case ui::PageType::SubredditPage:
                SetCurrentPostsSortType(Application::Get()->GetSubredditPostsSorting());
                break;
            case ui::PageType::UserPage:
                SetCurrentPostsSortType(Application::Get()->GetUserPostsSorting());
                SetCurrentCommentsSortType(Application::Get()->GetUserCommentsSorting());
                break;
            case ui::PageType::MultiRedditFeed:
                SetCurrentPostsSortType(Application::Get()->GetMultiPostsSorting());
                break;
            case ui::PageType::UpvotedPage:
            case ui::PageType::DownvotedPage:
            case ui::PageType::GildedPage:
            case ui::PageType::HiddenPage:
            case ui::PageType::SavedPage:
            case ui::PageType::WriteCommentPage:
            case ui::PageType::VideoPage:
            case ui::PageType::SearchPage:
            case ui::PageType::UserSubredditsPage:
            case ui::PageType::ImagePage:
            case ui::PageType::SettingsPage:
            case ui::PageType::SubmitPost:
            case ui::PageType::MultiReddits:
            case ui::PageType::EditMultiReddit:
            case ui::PageType::CreateMultiReddit:
                // ignore
                break;
        }
    }

    void HeaderBar::UpdateSortMenuOptions()
    {
        m_SortButton->set_menu_model(m_SortMenus[m_Page->GetSortType()]);
    }

    void HeaderBar::AddSortOptions(SortType sortType)
    {
        const Glib::RefPtr<Gio::Menu>& menu = m_SortMenus[sortType];
        switch (sortType)
        {
            case SortType::None:
                // do nothing
                break;
            case SortType::Posts:
            case SortType::SubredditPosts:
                AddPostsSortOptions(menu);
                break;
            case SortType::Comments:
                AddCommentsSortOptions(menu);
                break;
            case SortType::UserPosts:
                AddUserPostsSortOptions(menu);
                break;
            case SortType::UserComments:
                AddUserCommentsSortOptions(menu);
                break;
            case ui::SortType::MultiPosts:
                AddMultiPostsSortOptions(menu);
                break;
        }
    }

    void HeaderBar::AddPostsSortOptions(const Glib::RefPtr<Gio::Menu>& menu)
    {
        menu->append("Best", util::Helpers::FormatString("app.%s_sort(0)", m_Page->GetID().c_str()));
        menu->append("Hot", util::Helpers::FormatString("app.%s_sort(2)", m_Page->GetID().c_str()));
        menu->append("New", util::Helpers::FormatString("app.%s_sort(3)", m_Page->GetID().c_str()));
        menu->append("Controversial", util::Helpers::FormatString("app.%s_sort(1)", m_Page->GetID().c_str()));
        menu->append("Rising", util::Helpers::FormatString("app.%s_sort(5)", m_Page->GetID().c_str()));

        Glib::RefPtr<Gio::Menu> submenu = Gio::Menu::create();
        submenu->append("All Time", util::Helpers::FormatString("app.%s_sort(6)", m_Page->GetID().c_str()));
        submenu->append("This Year", util::Helpers::FormatString("app.%s_sort(7)", m_Page->GetID().c_str()));
        submenu->append("This Month", util::Helpers::FormatString("app.%s_sort(8)", m_Page->GetID().c_str()));
        submenu->append("This Week", util::Helpers::FormatString("app.%s_sort(9)", m_Page->GetID().c_str()));
        submenu->append("Today", util::Helpers::FormatString("app.%s_sort(10)", m_Page->GetID().c_str()));
        submenu->append("Now", util::Helpers::FormatString("app.%s_sort(11)", m_Page->GetID().c_str()));
        menu->append_submenu("Top", submenu);
    }

    void HeaderBar::AddUserPostsSortOptions(const Glib::RefPtr<Gio::Menu>& menu)
    {
        menu->append("Hot", util::Helpers::FormatString("app.%s_sort(2)", m_Page->GetID().c_str()));
        menu->append("New", util::Helpers::FormatString("app.%s_sort(3)", m_Page->GetID().c_str()));
        menu->append("Controversial", util::Helpers::FormatString("app.%s_sort(1)", m_Page->GetID().c_str()));

        Glib::RefPtr<Gio::Menu> submenu = Gio::Menu::create();
        submenu->append("All Time", util::Helpers::FormatString("app.%s_sort(6)", m_Page->GetID().c_str()));
        submenu->append("This Year", util::Helpers::FormatString("app.%s_sort(7)", m_Page->GetID().c_str()));
        submenu->append("This Month", util::Helpers::FormatString("app.%s_sort(8)", m_Page->GetID().c_str()));
        submenu->append("This Week", util::Helpers::FormatString("app.%s_sort(9)", m_Page->GetID().c_str()));
        submenu->append("Today", util::Helpers::FormatString("app.%s_sort(10)", m_Page->GetID().c_str()));
        submenu->append("Now", util::Helpers::FormatString("app.%s_sort(11)", m_Page->GetID().c_str()));
        menu->append_submenu("Top", submenu);
    }

    void HeaderBar::AddCommentsSortOptions(const Glib::RefPtr<Gio::Menu>& menu)
    {
        menu->append("Best", util::Helpers::FormatString("app.%s_sort(0)", m_Page->GetID().c_str()));
        menu->append("Top", util::Helpers::FormatString("app.%s_sort(1)", m_Page->GetID().c_str()));
        menu->append("New", util::Helpers::FormatString("app.%s_sort(2)", m_Page->GetID().c_str()));
        menu->append("Controversial", util::Helpers::FormatString("app.%s_sort(3)", m_Page->GetID().c_str()));
        menu->append("Old", util::Helpers::FormatString("app.%s_sort(4)", m_Page->GetID().c_str()));
        menu->append("Random", util::Helpers::FormatString("app.%s_sort(5)", m_Page->GetID().c_str()));
        menu->append("Q&amp;A", util::Helpers::FormatString("app.%s_sort(6)", m_Page->GetID().c_str()));
        menu->append("Live", util::Helpers::FormatString("app.%s_sort(7)", m_Page->GetID().c_str()));
    }

    void HeaderBar::AddUserCommentsSortOptions(const Glib::RefPtr<Gio::Menu>& menu)
    {
        menu->append("Top", util::Helpers::FormatString("app.%s_sort(1)", m_Page->GetID().c_str()));
        menu->append("New", util::Helpers::FormatString("app.%s_sort(2)", m_Page->GetID().c_str()));
        menu->append("Controversial", util::Helpers::FormatString("app.%s_sort(3)", m_Page->GetID().c_str()));
    }

    void HeaderBar::AddMultiPostsSortOptions(const Glib::RefPtr<Gio::Menu>& menu)
    {
        menu->append("Hot", util::Helpers::FormatString("app.%s_sort(2)", m_Page->GetID().c_str()));
        menu->append("New", util::Helpers::FormatString("app.%s_sort(3)", m_Page->GetID().c_str()));
        menu->append("Rising", util::Helpers::FormatString("app.%s_sort(5)", m_Page->GetID().c_str()));

        Glib::RefPtr<Gio::Menu> submenu = Gio::Menu::create();
        submenu->append("All Time", util::Helpers::FormatString("app.%s_sort(6)", m_Page->GetID().c_str()));
        submenu->append("This Year", util::Helpers::FormatString("app.%s_sort(7)", m_Page->GetID().c_str()));
        submenu->append("This Month", util::Helpers::FormatString("app.%s_sort(8)", m_Page->GetID().c_str()));
        submenu->append("This Week", util::Helpers::FormatString("app.%s_sort(9)", m_Page->GetID().c_str()));
        submenu->append("Today", util::Helpers::FormatString("app.%s_sort(10)", m_Page->GetID().c_str()));
        submenu->append("Now", util::Helpers::FormatString("app.%s_sort(11)", m_Page->GetID().c_str()));
        menu->append_submenu("Top", submenu);
    }

    void HeaderBar::SetCurrentPostsSortType(::api::PostsType type)
    {
        m_SortAction->change_state((gint32)type);
        SetTitle(api::PostsTypeToString(type));
    }

    void HeaderBar::SetCurrentCommentsSortType(::api::CommentsType type)
    {
        m_SortAction->change_state((gint32)type);
        SetTitle(api::CommentsTypeToString(type));
    }

    void HeaderBar::SetTitle(const std::string& title)
    {
        if (m_TitleLabel)
        {
            if (title.empty())
            {
                gtk_widget_set_visible((GtkWidget*)m_TitleLabel, false);
            }
            else
            {
                gtk_widget_set_visible((GtkWidget*)m_TitleLabel, true);
                adw_window_title_set_title(m_TitleLabel, title.c_str());
            }
        }
        else if (m_ViewSwitcherTitle)
        {
            adw_view_switcher_title_set_title(m_ViewSwitcherTitle, title.c_str());
        }
    }

    void HeaderBar::SetMenuButtonVisible(bool visible)
    {
        if (m_MenuButton)
        {
            m_MenuButton->set_visible(visible);
        }
    }

    AdwViewSwitcherTitle* HeaderBar::AddViewSwitcher(AdwViewStack* adwStack)
    {
        m_ViewSwitcherTitle = (AdwViewSwitcherTitle*)adw_view_switcher_title_new();
        adw_view_switcher_title_set_stack(m_ViewSwitcherTitle, adwStack);
        g_object_set(m_ViewSwitcherTitle, "margin-start", 10, "margin-end", 10, (char*)NULL);

        std::string oldTitle = adw_window_title_get_title(m_TitleLabel);
        adw_header_bar_set_title_widget((AdwHeaderBar*)m_HeaderBar->gobj(), (GtkWidget*)m_ViewSwitcherTitle);
        m_TitleLabel = nullptr;
        SetTitle(oldTitle);
        return m_ViewSwitcherTitle;
    }

    void HeaderBar::ShowSearchBar(bool show)
    {
        m_SearchBar->set_search_mode(show);
    }

    void HeaderBar::AddButton(const std::string& iconName, std::function<void()> cb, bool left)
    {
        Gtk::Button* button = new Gtk::Button();
        button->signal_clicked().connect(cb);
        button->set_icon_name(iconName);
        if (left)
        {
            adw_header_bar_pack_start((AdwHeaderBar*)m_HeaderBar->gobj(), (GtkWidget*)button->gobj());
        }
        else
        {
            adw_header_bar_pack_end((AdwHeaderBar*)m_HeaderBar->gobj(), (GtkWidget*)button->gobj());
        }
    }
}
