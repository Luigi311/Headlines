#pragma once

namespace ui
{
    class NotificationBanner
    {
    public:
        void OnAppActivate(const Glib::RefPtr<Gtk::Builder>& builder);
        void ShowNotification(const std::string& message);
    private:
        Gtk::Label* m_Label;
        Gtk::Button* m_Button;
        Gtk::Revealer* m_Revealer;
    };
}

