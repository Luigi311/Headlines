#pragma once

#include "pch.h"
#include "API/RedditAPI.h"
#include "UI/ui_types_fwd.h"

namespace ui
{
    class HeaderBar
    {
    public:
        HeaderBar(bool forPopup);
        ~HeaderBar();
        void CreateUI(const ui::Page* page, const std::vector<ui::SortType> sortTypes);

        void SetTitle(const std::string& title);
        void UpdateSortMenuOptions();
        void SetSortState(int state);
        void ShowSearchBar(bool show);
        void OnWindowFoldStateChanged();

        Gtk::Widget* GetHeaderBar() { return m_HeaderBar; }
        AdwViewSwitcherTitle* AddViewSwitcher(AdwViewStack* adwStack);
        void AddButton(const std::string& iconName, std::function<void()> cb, bool left);

        void SetMenuButtonVisible(bool visible);
        void SetCurrentPostsSortType(::api::PostsType type);
        void SetCurrentCommentsSortType(::api::CommentsType type);
    private:
        void SetupBackButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupMenuButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupPostButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupSortButton(const Glib::RefPtr<Gtk::Builder>& builder, const std::vector<ui::SortType> sortTypes);
        void SetupRefreshButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupSearchButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupSearchEntry();
        void SetupSendButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupTitle(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupDownloadButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupPageActions(const Glib::RefPtr<Gtk::Builder>& builder);

        void SetupSortActions();

        void AddSortOptions(SortType sortType);
        void AddPostsSortOptions(const Glib::RefPtr<Gio::Menu>& menu);
        void AddCommentsSortOptions(const Glib::RefPtr<Gio::Menu>& menu);
        void AddUserPostsSortOptions(const Glib::RefPtr<Gio::Menu>& menu);
        void AddUserCommentsSortOptions(const Glib::RefPtr<Gio::Menu>& menu);
        void AddMultiPostsSortOptions(const Glib::RefPtr<Gio::Menu>& menu);

        //can assume that m_Page is valid if this is valid.
        const ui::Page* m_Page;

        Gtk::Widget* m_HeaderBar;
        Gtk::Button* m_BackButton;
        Gtk::Button* m_MenuButton;
        Gtk::MenuButton* m_PostButton;
        Gtk::Button* m_SearchButton;
        Gtk::Button* m_SendButton;
        Gtk::MenuButton* m_SortButton;
        AdwWindowTitle* m_TitleLabel;
        Gtk::Button* m_RefreshButton;
        AdwViewSwitcherTitle* m_ViewSwitcherTitle;
        Gtk::Button* m_DownloadButton;
        Gtk::MenuButton* m_PageMenuButton;

        Gtk::SearchBar* m_SearchBar;
        Gtk::SearchEntry* m_SearchEntry;

        Glib::RefPtr<Gio::Menu> m_SubmitMenu;
        std::map<SortType, Glib::RefPtr<Gio::Menu>> m_SortMenus;
        Glib::RefPtr<Gio::Action> m_SortAction;

        sigc::connection m_SendButtonConnection;
        gulong m_FlapRevealConnection;
        gulong m_FlapFoldConnection;
        int m_LoginStatusChangedHandler;
        bool m_ForPopup;
    };
}

