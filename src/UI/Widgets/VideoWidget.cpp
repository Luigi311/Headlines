#include "VideoWidget.h"

namespace ui
{
    VideoWidget::VideoWidget(int width, int height, bool crop, bool blur)
            : PictureWidget(width, height, crop, blur)
    {

        Gtk::Image* image = new Gtk::Image();
        image->set_from_icon_name("media-playback-start-symbolic");
        image->set_pixel_size(150);
        image->set_parent(*this);

        m_Child = image;
    }
}
