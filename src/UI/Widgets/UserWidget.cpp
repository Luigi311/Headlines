#include "UserWidget.h"
#include "AppSettings.h"
#include "util/Helpers.h"
#include "API/RedditAPI.h"
#include "API/User.h"
#include "UI/Widgets/PictureWidget.h"

namespace ui
{
    UserWidget::~UserWidget()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }
    }

    void UserWidget::CreateUI(Gtk::Box* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/user.ui");

        m_UserPanel = builder->get_widget<Gtk::Box>("UserPanel");
        m_UserLabel = builder->get_widget<Gtk::Label>("UserLabel");
        m_KarmaLabel = builder->get_widget<Gtk::Label>("KarmaLabel");
        m_CakeDayLabel = builder->get_widget<Gtk::Label>("CreatedDateLabel");

        util::Helpers::ApplyCardStyle(m_UserPanel, AppSettings::Get()->GetBool("rounded_corners", false));
        m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool roundedCorners)
        {
            util::Helpers::ApplyCardStyle(m_UserPanel, roundedCorners);
        });

        Gtk::Box* subIconBox;
        subIconBox = builder->get_widget<Gtk::Box>("SubIconBox");
        Gtk::Requisition minSize, preferredSize;
        subIconBox->get_preferred_size(minSize, preferredSize);

        m_IconImageWidget = new PictureWidget(preferredSize.get_width(), preferredSize.get_height(), false, false);
        subIconBox->append(*m_IconImageWidget);

        m_FollowButton = builder->get_widget<Gtk::ToggleButton>("FollowButton");
        m_FollowButton->set_sensitive(api::RedditAPI::Get()->IsLoggedIn());

        parent->append(*m_UserPanel);
    }

    void UserWidget::SetUser(const api::UserRef& user)
    {
        m_User = user;

        m_UserLabel->set_text(m_User->GetDisplayNamePrefixed());
        m_FollowButton->set_active(m_User->UserIsSubscriber());
        m_FollowButton->set_label(m_FollowButton->get_active() ? "Unfollow" : "Follow");
        m_FollowButton->signal_toggled().connect([this]()
        {
            m_User->SetUserIsSubscriber(m_FollowButton->get_active());
            if (m_FollowButton->get_active())
            {
                api::RedditAPI::Get()->Subscribe(m_User);
                m_FollowButton->set_label("Unfollow");
            }
            else
            {
                api::RedditAPI::Get()->UnSubscribe(m_User);
                m_FollowButton->set_label("Follow");
            }
        });

        {
            std::stringstream ss;
            ss << "Karma: " << m_User->GetKarma();

            m_KarmaLabel->set_text(ss.str());
        }

        {
            std::stringstream ss;
            ss << "Cake day: " << util::Helpers::TimeStampToDateString(m_User->GetCakeDayTimeStamp());

            m_CakeDayLabel->set_text(ss.str());
        }

        m_IconImageWidget->SetImageData(util::ImageData(m_User->GetIconPath()));
    }

    void UserWidget::SetVisible(bool visible)
    {
        m_UserPanel->set_visible(visible);
    }
}