#pragma once

#include "API/api_fwd.h"

namespace ui
{
    class MultiRedditBanner
    {
    public:
        static Gtk::Box* CreateMultiRedditBanner(Gtk::Box* parent, api::MultiRedditRef& multi, int& roundedCornersSignalHandler);
    };
}