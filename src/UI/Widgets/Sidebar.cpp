#include "Sidebar.h"
#include "Application.h"
#include "UI/Pages/UserPage.h"
#include "API/RedditAPI.h"
#include "UI/Pages/DownvotedPostsPage.h"
#include "UI/Pages/UpvotedPostsPage.h"
#include "UI/Pages/GildedPostsPage.h"
#include "UI/Pages/HiddenPostsPage.h"
#include "UI/Popups/UserSubredditsPopup.h"
#include "UI/Pages/SettingsPage.h"
#include "UI/Pages/UserMultiRedditsPage.h"
#include "HeaderBar.h"

namespace ui
{
    void Sidebar::OnAppActivate(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::ListBox* list;
        list = builder->get_widget<Gtk::ListBox>("SidebarList");
        list->signal_row_activated().connect([this](Gtk::ListBoxRow* row)
                                             {
                                                if (VERIFY(m_ItemCallbacks.count(row) > 0, "No callback setup for sidebar item"))
                                                {
                                                    if (adw_flap_get_folded(Application::Get()->GetFlap()))
                                                    {
                                                        adw_flap_set_reveal_flap(Application::Get()->GetFlap(), false);
                                                    }
                                                    m_ItemCallbacks[row]();
                                                }
                                             });


        m_ProfileItem = builder->get_widget<Gtk::ListBoxRow>("ProfileListItem");
        m_ItemCallbacks.insert({m_ProfileItem, []()
        {
            ui::UserPageRef userView = std::make_shared<ui::UserPage>();
            Application::Get()->AddPage(userView);
        }});

        m_LogoutItem = builder->get_widget<Gtk::ListBoxRow>("LogoutListItem");
        m_ItemCallbacks.insert({m_LogoutItem, []()
        {
            api::RedditAPI::Get()->Logout();
        }});

        m_LoginItem = builder->get_widget<Gtk::ListBoxRow>("LoginListItem");
        m_ItemCallbacks.insert({m_LoginItem, []()
        {
            api::RedditAPI::Get()->Login();
        }});

        m_LoginSeparator = builder->get_widget<Gtk::ListBoxRow>("LoginSeparator");

        m_UserSubredditsItem = builder->get_widget<Gtk::ListBoxRow>("UserSubredditsListItem");
        m_ItemCallbacks.insert({m_UserSubredditsItem, []()
        {
            ui::UserSubredditsPopupRef subredditsPage = std::make_shared<ui::UserSubredditsPopup>(nullptr, true);
            Application::Get()->AddPage(subredditsPage);
        }});

        m_UpvotedItem = builder->get_widget<Gtk::ListBoxRow>("UpvotedListitem");
        m_ItemCallbacks.insert({m_UpvotedItem, []()
        {
            ui::UpvotedPostsPageRef upvotedPage = std::make_shared<ui::UpvotedPostsPage>();
            Application::Get()->AddPage(upvotedPage);
            upvotedPage->GetHeaderBar()->SetTitle("Upvoted");
        }});

        m_DownvotedItem = builder->get_widget<Gtk::ListBoxRow>("DownvotedListItem");
        m_ItemCallbacks.insert({m_DownvotedItem, []()
        {
            ui::DownvotedPostsPageRef downvotedPage = std::make_shared<ui::DownvotedPostsPage>();
            Application::Get()->AddPage(downvotedPage);
            downvotedPage->GetHeaderBar()->SetTitle("Downvoted");
        }});

        m_HiddenItem = builder->get_widget<Gtk::ListBoxRow>("HiddenListItem");
        m_ItemCallbacks.insert({m_HiddenItem, []()
        {
            ui::HiddenPostsPageRef hiddenPage = std::make_shared<ui::HiddenPostsPage>();
            Application::Get()->AddPage(hiddenPage);
            hiddenPage->GetHeaderBar()->SetTitle("Hidden");
        }});

        m_GildedItem = builder->get_widget<Gtk::ListBoxRow>("GildedListItem");
        m_ItemCallbacks.insert({m_GildedItem, []()
        {
            ui::GildedPostsPageRef gildedPage = std::make_shared<ui::GildedPostsPage>();
            Application::Get()->AddPage(gildedPage);
            gildedPage->GetHeaderBar()->SetTitle("Gilded");
        }});

        m_MultiredditItem = builder->get_widget<Gtk::ListBoxRow>("MultiRedditListItem");
        m_ItemCallbacks.insert({m_MultiredditItem, []()
        {
            ui::UserMultiRedditsPageRef userMultiRedditsPage = std::make_shared<ui::UserMultiRedditsPage>();
            Application::Get()->AddPage(userMultiRedditsPage);
            userMultiRedditsPage->GetHeaderBar()->SetTitle("Multireddit");
        }});

        m_SavedItem = builder->get_widget<Gtk::ListBoxRow>("SavedListitem");
        m_ItemCallbacks.insert({m_SavedItem, []()
        {
            Application::Get()->AddPage(std::make_shared<ui::SavedPage>());
        }});

        Gtk::ListBoxRow* settingsRow;
        settingsRow = builder->get_widget<Gtk::ListBoxRow>("SettingsListItem");
        m_ItemCallbacks.insert({ settingsRow, []()
        {
            ui::SettingsPageRef settingsPage = std::make_shared<ui::SettingsPage>();
            Application::Get()->AddPage(settingsPage);
            settingsPage->GetHeaderBar()->SetTitle("Settings");
        }});

        m_HeaderBar = builder->get_widget<Gtk::Widget>("SideBarHeader");
        g_object_bind_property(Application::Get()->GetFlap(), "folded", m_HeaderBar->gobj(), "visible", GBindingFlags::G_BINDING_INVERT_BOOLEAN);
        m_MenuButton = builder->get_widget<Gtk::Button>("SideBarMenuButton");
        m_MenuButton->signal_clicked().connect([]()
        {
            adw_flap_set_reveal_flap(Application::Get()->GetFlap(), !adw_flap_get_reveal_flap(Application::Get()->GetFlap()));
        });

        UpdateMenuItems();
    }

    void Sidebar::UpdateMenuItems()
    {
        bool loggedIn = api::RedditAPI::Get()->IsLoggedIn();
        m_LoginItem->set_visible(!loggedIn);
        m_LogoutItem->set_visible(loggedIn);
        m_ProfileItem->set_visible(loggedIn);
        m_LoginSeparator->set_visible(loggedIn);
        m_UpvotedItem->set_visible(loggedIn);
        m_DownvotedItem->set_visible(loggedIn);
        m_SavedItem->set_visible(loggedIn);
        m_GildedItem->set_visible(loggedIn);
        m_HiddenItem->set_visible(loggedIn);
        m_UserSubredditsItem->set_visible(loggedIn);
        m_MultiredditItem->set_visible(loggedIn);
    }
    }