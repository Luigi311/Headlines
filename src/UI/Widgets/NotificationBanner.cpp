#include "NotificationBanner.h"

namespace ui
{
    void NotificationBanner::OnAppActivate(const Glib::RefPtr<Gtk::Builder> &builder)
    {
        m_Revealer = builder->get_widget<Gtk::Revealer>("NotificationRevealer");
        m_Label = builder->get_widget<Gtk::Label>("NotificationLabel");
        m_Button = builder->get_widget<Gtk::Button>("NotificationQuitButton");
        m_Button->signal_clicked().connect([this]()
        {
            m_Revealer->set_reveal_child(false);
        });
    }

    static int s_NotificationCount = 0;
    static gboolean closeNotification(gpointer data)
    {
        if (s_NotificationCount == 1)
        {
            Gtk::Revealer *revealer = (Gtk::Revealer *) data;
            revealer->set_reveal_child(false);
            s_NotificationCount--;
        }
        else
        {
            s_NotificationCount--;
        }

        return false;
    }

    void NotificationBanner::ShowNotification(const std::string& message)
    {
        m_Label->set_text(message);
        m_Revealer->set_reveal_child(true);
        s_NotificationCount++;
        g_timeout_add_seconds(3, closeNotification, m_Revealer);
    }
}