#include <util/Helpers.h>
#include "Application.h"
#include "UI/Pages/Page.h"
#include "UI/Widgets/HeaderBar.h"
#include "UI/Pages/SearchPage.h"
#include "UI/Widgets/NotificationBanner.h"
#include "API/RedditAPI.h"
#include "AppSettings.h"
#include "UI/Pages/MainFeedPage.h"
#include "UI/Popups/Popup.h"
#include <curl/curl.h>

Application* Application::s_Instance;

Application::Application()
          : m_Sidebar(std::make_shared<ui::Sidebar>())
          , m_NotificationBanner(std::make_shared<ui::NotificationBanner>())
          , m_GtkApp(nullptr)
          , m_HttpClient()
          , m_Stack()
          , m_Window()
          , m_PageStack()
          , m_PostFeedSorting(api::PostsType::Best)
          , m_UserPostsSorting(api::PostsType::New)
          , m_SubredditPostsSorting(api::PostsType::Best)
          , m_MultiPostsSorting(api::PostsType::Hot)
          , m_CommentsViewSorting(api::CommentsType::Confidence)
          , m_UserCommentsSorting(api::CommentsType::New)
{

}

int Application::Run(int argc, char** argv)
{
    s_Instance = new Application();
    s_Instance->Init();
    int ret = s_Instance->GetGtkApplication()->run(argc, argv);

    s_Instance->Shutdown();
    Destroy();

    return ret;
}

void Application::Shutdown()
{
    m_Sidebar.reset();
    m_PageStack.clear();

    delete m_Window;

    api::RedditAPI::Destroy();
}

Application* Application::Get()
{
    return s_Instance;
}

void Application::Destroy()
{
    curl_global_cleanup();
    delete s_Instance;
    s_Instance = nullptr;
}

static void leaflet_notify(AdwLeaflet*, GParamSpec*, gpointer)
{
    Application::Get()->OnPageChanged();
}

static void window_size_changed(AdwWindow* window, GParamSpec* pspec, gpointer)
{
    if (strcmp(pspec->name, "default-width") == 0)
    {
        int width, height;
        gtk_window_get_default_size((GtkWindow*)window, &width, &height);
        AppSettings::Get()->SetInt("default_window_width", width);
    }
    else if (strcmp(pspec->name, "default-height") == 0)
    {
        int width, height;
        gtk_window_get_default_size((GtkWindow*)window, &width, &height);
        AppSettings::Get()->SetInt("default_window_height", height);
    }
}


void Application::OnAppActivate()
{
    if (AppSettings::Get()->GetBool("use_dark_theme", false))
    {
        adw_style_manager_set_color_scheme(adw_style_manager_get_default(), ADW_COLOR_SCHEME_PREFER_DARK);
    }
    else
    {
        adw_style_manager_set_color_scheme(adw_style_manager_get_default(), ADW_COLOR_SCHEME_DEFAULT);
    }

    auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/main_window.ui");

    m_Window = builder->get_widget<Gtk::Window>("window1");
    g_signal_connect(m_Window->gobj(), "notify", G_CALLBACK(window_size_changed), nullptr);
    m_Window->set_default_size(AppSettings::Get()->GetInt("default_window_width", 1280), AppSettings::Get()->GetInt("default_window_height", 800));

    m_Stack = (AdwLeaflet*)builder->get_widget<Gtk::Widget>("MainPageStack")->gobj();
    adw_leaflet_set_can_unfold(m_Stack, false);
    g_signal_connect(m_Stack, "notify::visible-child", G_CALLBACK(leaflet_notify), nullptr);

    m_Flap = (AdwFlap*)builder->get_widget<Gtk::Widget>("flap")->gobj();
    if (!adw_flap_get_folded(m_Flap))
    {
        adw_flap_set_reveal_flap(m_Flap, AppSettings::Get()->GetBool("flap_revealed", true));
    }

    m_GtkApp->add_window(*m_Window);

    auto css_provider = Gtk::CssProvider::create();
    css_provider->load_from_resource("/io/gitlab/caveman250/headlines/style.css");

    Gtk::StyleContext::add_provider_for_display(Gdk::Display::get_default(), css_provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    m_NotificationBanner->OnAppActivate(builder);
    m_Sidebar->OnAppActivate(builder);

    AddPage(std::make_shared<ui::MainFeedPage>());
    m_Window->show();
}

void Application::Init()
{
    curl_global_init(CURL_GLOBAL_ALL);

    //force initialize the api.
    api::RedditAPI::Get();

#if !WIN32
    //ensure directories exist
    std::string configDir = util::Helpers::FormatString("%s/headlines", Glib::get_user_config_dir().c_str());
    std::string cacheDir = util::Helpers::FormatString("%s/headlines", Glib::get_user_cache_dir().c_str());

    if (!std::filesystem::exists(configDir))
    {
        std::filesystem::create_directory(configDir);
    }
    if (!std::filesystem::exists(cacheDir))
    {
        std::filesystem::create_directory(cacheDir);
    }
#endif

    m_GtkApp = Gtk::Application::create("io.gitlab.caveman250.headlines");
    m_GtkApp->signal_activate().connect([this]()
                                        {
                                            OnAppActivate();
                                        });
}

void Application::AddPage(ui::PageRef page)
{
    PushPageStack(page);
    page->CreateUI(m_Stack);
}

void Application::SetCurrentPostsSortType(api::PostsType type)
{
    switch (GetActivePage()->GetPageType())
    {
        case ui::PageType::MainFeedPage:
            m_PostFeedSorting = type;
            GetActivePage()->Reload();
            break;
        case ui::PageType::UserPage:
            m_UserPostsSorting = type;
            GetActivePage()->Reload();
            break;
        case ui::PageType::SubredditPage:
            m_SubredditPostsSorting = type;
            GetActivePage()->Reload();
            break;
        case ui::PageType::MultiRedditFeed:
            m_MultiPostsSorting = type;
            GetActivePage()->Reload();
            break;
        case ui::PageType::SavedPage:
        case ui::PageType::UpvotedPage:
        case ui::PageType::DownvotedPage:
        case ui::PageType::GildedPage:
        case ui::PageType::HiddenPage:
        case ui::PageType::CommentsPage:
        case ui::PageType::WriteCommentPage:
        case ui::PageType::VideoPage:
        case ui::PageType::SearchPage:
        case ui::PageType::UserSubredditsPage:
        case ui::PageType::ImagePage:
        case ui::PageType::SettingsPage:
        case ui::PageType::SubmitPost:
        case ui::PageType::MultiReddits:
        case ui::PageType::EditMultiReddit:
        case ui::PageType::CreateMultiReddit:
            // ignore
            break;
    }
}

void Application::SetCurrentCommentsSortType(api::CommentsType type)
{
    switch (GetActivePage()->GetPageType())
    {
        case ui::PageType::CommentsPage:
            m_CommentsViewSorting = type;
            GetActivePage()->Reload();
            break;
        case ui::PageType::UserPage:
            m_UserCommentsSorting = type;
            GetActivePage()->Reload();
            break;
        case ui::PageType::SavedPage:
        case ui::PageType::MainFeedPage:
        case ui::PageType::SubredditPage:
        case ui::PageType::UpvotedPage:
        case ui::PageType::DownvotedPage:
        case ui::PageType::GildedPage:
        case ui::PageType::HiddenPage:
        case ui::PageType::WriteCommentPage:
        case ui::PageType::VideoPage:
        case ui::PageType::SearchPage:
        case ui::PageType::UserSubredditsPage:
        case ui::PageType::ImagePage:
        case ui::PageType::SettingsPage:
        case ui::PageType::SubmitPost:
        case ui::PageType::MultiReddits:
        case ui::PageType::MultiRedditFeed:
        case ui::PageType::EditMultiReddit:
        case ui::PageType::CreateMultiReddit:
            // ignore
            break;
    }
}

void Application::ShowNotification(const std::string& message)
{
    m_NotificationBanner->ShowNotification(message);
}

void Application::ShowPage(const ui::Page* page)
{
    if (page)
    {
        adw_leaflet_set_visible_child_name(m_Stack, page->GetID().c_str());
    }
    else
    {
        adw_leaflet_set_visible_child_name(m_Stack, "MainFeedPage");
    }
}

ui::PageRef Application::GetActivePage()
{
    const char* id = adw_leaflet_get_visible_child_name(m_Stack);
    auto it = std::find_if(m_PageStack.begin(), m_PageStack.end(), [id](const ui::PageRef& page) { return page->GetID() == id; });
    if (it != m_PageStack.end())
    {
        return *it;
    }

    return ui::PageRef();
}

void Application::PushPageStack(const ui::PageRef &page)
{
    if (m_PageStack.size() > 1 && (!page->IsPopup() || std::static_pointer_cast<ui::Popup>(page)->CreatedAsPage() || adw_flap_get_folded(m_Flap)))
    {
        // get the index of the page that requested a new page
        const char* currPageName = adw_leaflet_get_visible_child_name(m_Stack);
        auto currPageIt = std::find_if(m_PageStack.begin(), m_PageStack.end(), [currPageName](const ui::PageRef& page) { return page->GetID() == currPageName; });
        if (currPageIt == m_PageStack.end())
        {
            g_critical("Page %s requested a new page but it is not in the page stack.", currPageName);
        }

        //new series of pages, delete old ones
        for (auto it = currPageIt + 1; it != m_PageStack.end(); ++it)
        {
            ui::PageRef stalePage = (*it);
            stalePage->Cleanup();
            if (GtkWidget* oldPage = adw_leaflet_get_child_by_name(m_Stack, stalePage->GetID().c_str()))
            {
                adw_leaflet_remove(m_Stack, oldPage);
            }
        }

        m_PageStack.erase(currPageIt + 1, m_PageStack.end());
    }

    m_PageStack.push_back(page);
}

void Application::NavigateBackwards()
{
    adw_leaflet_navigate(m_Stack, AdwNavigationDirection::ADW_NAVIGATION_DIRECTION_BACK);
}

void Application::OnPageChanged()
{
    const char* currPageName = adw_leaflet_get_visible_child_name(m_Stack);

    if (!currPageName)
    {
        return;
    }

    auto currPageIt = std::find_if(m_PageStack.begin(), m_PageStack.end(), [currPageName](const ui::PageRef& page) { return page->GetID() == currPageName; });
    if (currPageIt == m_PageStack.end())
    {
        g_critical("Application::OnNavigateBackwards() - No current page.");
    }

    for (auto it = currPageIt + 1; it != m_PageStack.end(); ++it)
    {
        ui::PageRef page = (*it);
        page->OnDeactivate();
    }

    (*currPageIt)->OnActivate();
}

std::vector<ui::PageRef> Application::GetAllPagesOfType(ui::PageType type)
{
    std::vector<ui::PageRef> ret;

    for (const ui::PageRef& page : m_PageStack)
    {
        if (page->GetPageType() == type)
        {
            ret.push_back(page);
        }
    }

    return ret;
}

ui::PopupRef Application::GetPopup(bool ignoreLast) const
{
    const ui::PageRef& page = !ignoreLast ? m_PageStack.back() : m_PageStack[m_PageStack.size() - 2];
    if (!page->IsPopup())
    {
        return ui::PopupRef();
    }
    else
    {
        const ui::PopupRef& popup = std::static_pointer_cast<ui::Popup>(page);
        return !popup->GetDialog() ? ui::PopupRef() : popup;
    }
}
